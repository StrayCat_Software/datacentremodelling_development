import in.labulle.anycode.uml.*;

/**
 * This class is used by the Groovy script JavaScript-content.mda to allow the 
 * astah anycode plugin to generate JavaScript "class" files as output, based 
 * on a JavaScript model.
 * 
 * @author Daniel Pawsey
 */

class JavascriptDirective{

	/**
	* Generates class documentation.
	*/
	def generateClassDocumentation(IClassifier classifier){
		String documentation = "/**";
		documentation = documentation + "\n* " + classifier.getDocumentation();
		documentation = documentation + "\n*";
		documentation = documentation + "\n* This JavaScript pseudo-class was automatically generated using Astah ";
		documentation = documentation + "\n* with the anycode plugin, using a groovy template written by Daniel Pawsey.";
		documentation = documentation + "\n*";
		documentation = documentation + "\n* @author Daniel Pawsey";
		documentation = documentation + "\n*/";
		
		return documentation;
	}
	
	/**
	* Generates class declaration, including whether this class is a subclass, by extending the prototype of
	* it's superclass.
	*
	* Note that this mechanism is intended only to support single-parent inheritance.
	*/
	def generateClassDeclaration(IClassifier classifier){

		String output = "";
		ArrayList superclasses = classifier.getGeneralizations();

		// If there are any generalisations, declare them in the class declaration.
		if(superclasses.size() >= 1){
			output = superclasses.get(0).getName() + ".prototype." + classifier.getName() + " = function(){";
		}
		else {
			output = "var " + classifier.getName() + " = function(){";
		}


		return output;
	}
	
	/**
	* Generates a comment-list of dependency classes for the output class
	*/
	def generateDependenciesList(IClassifier classifier){
		String output = "";
		
		
		// Get the list of dependency classes
		List dependencies = classifier.getClientDependencies();
		
		// If there are any dependencies, generate the comment.
		if(dependencies.size() >= 1){
			output = output + "\n/*";
			output = output + "\n* The following are dependencies which this class relies upon:";
			
			// iterate through the dependencies, and add them to the comment.
			for(int counter = 0; counter <= dependencies.length(); counter++){
				output = output + "\n*" + dependencies.get(counter).getName();
			}
			
			output = output + "\n*/";
		}
		
		return output;
	}
	
	/**
	* Determines whether an attribute is publicly visible or otherwise, and whether it has any documentation,
	* and adds it to the output file accordingly.
	*/
	def attribute(IAttribute attribute){

		String output = "\n	/**";
		
		// If the attribute is public, create it as a global variable.
		if(attribute.getVisibility().toString().equals("PUBLIC")){
		
			// Document that the variable is public.
			output = output + "\n	* var " + attribute.getName() + " is public.";
			output = output + "\n	*/";
			
			// Add documentation if any exists.
			if(!attribute.getDocumentation().equals("")){
				output = output + "\n	* " + attribute.getDocumentation();
				output = output + "\n	*/";
			}
			
			// Add the global public variable variable.
			output = output + "\n	var " + attribute.getName() + ";";
		} 
		else {
			
			// Add documentation if any exists.
			if(!attribute.getDocumentation().equals("")){
				output = output + "\n	* " + attribute.getDocumentation();
			}
			
			// Determine that the attribute is private.
			output = output + "\n	* @private";
			output = output + "\n	*/";
			
			// Add the attribute itself
			output = output + "\n	this." + attribute.getName() + ";";
			
			// Create a setter for the attribute.
			output = output + "\n" + makeSetter(attribute);
			
			// Create a getter for the attribute.
			output = output + "\n" + makeGetter(attribute);
		}
		
		return output;
	}
	
	/**
	* Creates a setter for the attribute.
	*/
	def makeSetter(IAttribute attribute){
		
		String output = "";
		
		output = output + "\n	/**";
		output = output + "\n	* Setter for " + attribute.getName() + ".";
		output = output + "\n	*\n	* @this{" + attribute.getOwner().getName() + "}";
		output = output + "\n	*/";
			
		output = output + "\n	" + attribute.getOwner().getName() + ".prototype.set" + capitaliseFirstLetter(attribute.getName()) + " = function (" + attribute.getName() + ") {";
		output = output + "\n		this." + attribute.getName() + " = " + attribute.getName() + ";";
		output = output + "\n	}";
		
		return output;
	}
	
	/**
	* Creates a getter for the attribute.
	*/
	def makeGetter(IAttribute attribute){
	
		String output = "";
		
		output = output + "\n\n	/**"
		output = output + "\n	* Getter for " + attribute.getName() + ".";
		output = output + "\n	*\n	* @this{" + attribute.getOwner().getName() + "}";
		output = output + "\n	*/";

		output = output + "\n	" + attribute.getOwner().getName() + ".prototype.get" + capitaliseFirstLetter(attribute.getName()) + " = function () {";
		output = output + "\n		return this." + attribute.getName() + ";";
		output = output + "\n	}";
		
		return output;
	}
	
	/**
     * Capitalize the first letter in a string.
     */
    def capitaliseFirstLetter(String s) {
		
		if(s.length() >= 1){
            return s.substring(0,1).toUpperCase() + s.substring(1);
        }
        else
        {
           return null;
        }
		
        //return s[0].toUpperCase() + s.substring(1)
    }
	
	/**
     * Adds the IOperation to the class. 
     * calls listParameters in order to 
     * add the operation's parameters to 
     * the output.
	 *
	 * Also adds documentation before the
	 * operation body if any documentation
	 * exists.
     *
     * @param operation
     *     The IOperation to be added to 
     *     the class.
     * @returns
     *     The JavaScript function to be 
     *     be added to the output class.
     */
    def operation(IOperation operation) {
	
		List parameterList = operation.getParameters();
		
		String parameterListString = "";

        // Iterate through all of the parameters.
        for (int counter = 0; counter <= parameterList.size() - 1; counter++) {

            // If there is more than one parameter 
            // in total, separate them with a comma.
            if (counter == 0) {
                parameterListString = parameterListString + parameterList.get(counter).getName();

            } else {
                parameterListString = parameterListString + ", " + parameterList.get(counter).getName();
            }
        }

		String output = "";
		
		// Add documentation if any exists.
		if(!operation.getDocumentation().equals("")){
			output = "/**";
			output = output + "\n	* " + operation.getDocumentation();
			output = output + "\n	*\n	* @this {" + operation.getOwner().getName() + "}";
			output = output + "\n	*/";
		}
		
		// Add the method declaration and body.
		output = output + "\n	" + operation.getOwner().getName() + ".prototype." + operation.getName() + " = function (" + parameterListString + ") {"
		output = output + "\n		// TODO: Implement method " + operation.getName();
		output = output + "\n	}";
		
		return output;
	}
}