	{

		"metadata": {
			"formatVersion" : 3.1,
			"generatedBy"   : "Blender 2.7 Exporter",
			"vertices"      : 16,
			"faces"         : 12,
			"normals"       : 8,
			"colors"        : 0,
			"uvs"           : [],
			"materials"     : 1,
			"morphTargets"  : 0,
			"bones"         : 0
		},

			"scale" : 1.000000,
			
			"vertices" : [2.719,0.0726354,-3.12742,2.719,0.0726354,1.69446,-2.10287,0.0726354,1.69446,-2.10287,0.0726354,-3.12742,2.71901,1.37002,-3.12742,2.719,1.37002,1.69446,-2.10287,1.37002,1.69446,-2.10287,1.37002,-3.12742,-1.84939,-0.189404,1.49347,-1.84939,-0.189404,-2.92334,2.51185,-0.189404,-2.92334,2.51185,-0.189404,1.49347,-1.84939,0.0682077,1.49347,-1.84939,0.0682077,-2.92334,2.51185,0.0682077,-2.92334,2.51185,0.0682077,1.49347],
			"faces"    : [35,0,1,2,3,0,0,1,2,3,35,4,7,6,5,0,4,5,6,7,35,0,4,5,1,0,0,4,7,1,35,1,5,6,2,0,1,7,6,2,35,2,6,7,3,0,2,6,5,3,35,4,0,3,7,0,4,0,3,5,35,12,13,9,8,0,6,5,3,2,35,13,14,10,9,0,5,4,0,3,35,14,15,11,10,0,4,7,1,0,35,15,12,8,11,0,7,6,2,1,35,8,9,10,11,0,2,3,0,1,35,15,14,13,12,0,7,4,5,6],
			"uvs"      : [],
			"normals"  : [0.577349,-0.577349,-0.577349,0.577349,-0.577349,0.577349,-0.577349,-0.577349,0.577349,-0.577349,-0.577349,-0.577349,0.577349,0.577349,-0.577349,-0.577349,0.577349,-0.577349,-0.577349,0.577349,0.577349,0.577349,0.577349,0.577349],
			
			"skinIndices"  : [],
			"skinWeights"  : [],
			"morphTargets" : [],
			
			"bones"      : [],
			"animations" : [],
			
			"colors"    : [],
			"materials" : [
				{
					"DbgColor": 15658734,
					"DbgIndex": 0,
					"DbgName": "Material",
					"blending": "NormalBlending",
					"colorAmbient": [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
					"colorDiffuse": [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
					"colorEmissive": [0.0, 0.0, 0.0],
					"colorSpecular": [0.5, 0.5, 0.5],
					"depthTest": true,
					"depthWrite": true,
					"shading": "Lambert",
					"specularCoef": 50,
					"transparency": 1.0,
					"transparent": false,
					"vertexColors": false
				}
			]


	}
