## Data Centre Modelling README ##

A live version of the site can be found at http://cmp-14javapro01.cmp.uea.ac.uk/DataCentreModelling

## Set up ##

**1) JDBC Driver **
You will need the use the JDBC driver to use this web-application. You can find it in the bin folder of your PostgreSQL installation. A copy is also included in the 'libs' folder of this repository. In netbeans, you can add it to your project by right clicking it, going to 'project properties', then 'libraries', clicking the 'Add JAR' button then navigating to the PostgreSQL JDBC driver JAR file.

**2) Database connection **
The database connection credentials must be written into the Java file *DatabaseConnection_Key.java*. If and only if you plan to connect the web app to a different server, you will have to modify the following variables in the following ways:
*databaseType* needs to be modified to be the correct type of database. The default is "postgresql", but if you are using something other than that, write it's name into this string.
*hostName* needs to be the url of the server hosting your database.
*databaseName* needs to be the name of your database.
*databaseUser* needs to be the user name used to access your database
*databasePassword* needs to be the password for your database user used to access the datbase.

## Signing into the Administrator Area ##

To sign in to see the add and delete objects page, first click the "sign-in" button, then use the following credentials:

username: wxu13keu

password: abc123

## Documentation ##
API documentation for the server-side Java app can be found under **DataCentreModelling/dist/javadoc**. This was generated using Netbeans 8.0.2.

API documentation for the client-side JavaScript app can be found under **DataCentreModelling/dist/out** This was generated via jsdoc via node.js and https://www.npmjs.com/package/jsdoc

## Database ##

If you need to generate the tables and data on a new database, you can do using the SQL DDL script "0_FullDatabaseScript.sql" at Database/DDL.

**IMPORTANT NOTE ON ADDING RECORDS:** Records must not have spaces in their names, otherwise they will not be parsed correctly. If you need to add an object with a mult-word name like 'Air Conditioner Type 2', name it using underscores, like 'Air_Conditioner_Type_2'.

## 3D Models ##

All 3D models have been made by myself using Blender, and exported using the three.js JSON exporter utility. The utility was not created by myself, it is available at https://github.com/mrdoob/three.js/