package utils;

import java.util.ArrayList;
import mvc.controller.DatabaseTools.DatabaseReader;

/**
 * Utilities class for activities required by AddObjectForm.jsp.
 *
 * @author Daniel Pawsey
 * @since 03/08/2015
 */
public class AddObjectForm_Controller {

    private DatabaseReader dbReader;

    public AddObjectForm_Controller() {
        dbReader = new DatabaseReader();
    }

    /**
     * @return The names of all of the tables in the database (excluding the
     * administrator table), as a drop down list.
     */
    public String getTablesAsDropdownList() {

        String dropdownList = "<select name=\"type\">";
        ArrayList<String> tableList = dbReader.getTableList();

        for (String table : tableList) {
            if (!table.equals("administrator")) {
                dropdownList += "<option value=\"" + table + "\">" + table + "</option>";
            }
        }

        dropdownList += "</select>";

        return dropdownList;
    }
}
