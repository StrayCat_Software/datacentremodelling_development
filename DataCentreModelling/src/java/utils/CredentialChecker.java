package utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mvc.controller.DatabaseTools.DatabaseConnection;

/**
 * Check whether the supplied user-name and password both exactly match a record
 * in the administrator table. This handles the sign-in functionality of the
 * site.
 *
 * @author Daniel Pawsey
 * @since 23/07/2015
 */
public class CredentialChecker {

    private DatabaseConnection dbAccessor;

    public CredentialChecker() {
        dbAccessor = new DatabaseConnection();
    }

    /**
     * Check whether the supplied user-name and password both exactly match a
     * record in the administrator table.
     *
     * @param username The username to be checked, as a String.
     * 
     * @param password the password to be checked, as a String.
     * 
     * @return A boolean, assigned to true if the values both exactly match a
     * record in the administrator table, or false for any other condition.
     */
    public boolean checkCredentials(String username, String password) {
        boolean goodCredentials = false;

        try {
            ResultSet resultSet = dbAccessor.query("SELECT username FROM administrator WHERE username = '" + username + "' AND password = '" + password + "';");

            while (resultSet.next()) {
                goodCredentials = true;
            }

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(CredentialChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return goodCredentials;
    }

}
