package utils;

import java.util.ArrayList;

/**
 * General shared functionality methods for controller package classes.
 * 
 * @author Daniel Pawsey
 * @since 05/08/2015
 */
public class ControllerUtils {
    
    /**
     * Capitalises the first letter of a supplied string. Called by
     * {@link utils.ControllerUtils#capitaliseFirstLetter(java.lang.String)}.
     *
     * @param input The String who's first letter is to be capitalised
     *
     * @return The input string with it's first letter capitalised.
     */
    public String capitaliseFirstLetter(String input) {

        String output;

        // Convert the first letter to upper case
        String firstLetter = "" + input.charAt(0);
        firstLetter = firstLetter.toUpperCase();

        // concatenate the new capitalised first letter onto the rest of the 
        // string.
        output = firstLetter;
        char[] inputCharArray = input.toCharArray();

        for (int i = 1; i <= inputCharArray.length - 1; i++) {
            output += inputCharArray[i];
        }

        return output;
    }
    
    /**
     * Removes a specified String from a specified ArrayList of Strings.
     * 
     * @param inputList The list to be filtered
     * @param filterString The String to filter out of inputList
     * @return inputList, with filterString removed.
     */
    public ArrayList<String> filterOut(ArrayList<String> inputList, String filterString) {
        ArrayList<String> filteredList = new ArrayList<>();
        for (String value : inputList) {
            if (!value.equals(filterString)){
                filteredList.add(value);
            }
        }
        return filteredList;
    }
    
}
