/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import javax.servlet.http.HttpServletRequest;

/**
 * General utilities class for web related tasks.
 *
 * @author Daniel Pawsey
 */
public class SiteUtils {

    /**
     * Get the full URL of the page from which this method is called as a
     * String.
     *
     * @param request The HttpRequest of the calling page.
     * 
     * @return The full URL of the page from which this method is called as a
     * String.
     */
    public String getURL(HttpServletRequest request) {
        return request.getScheme()
                + "://"
                + request.getServerName()
                + ":"
                + request.getServerPort()
                + request.getContextPath()
                + "/";
    }
}
