package ServletPackage;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mvc.controller.DatabaseTools.DatabaseWriter;
import mvc.controller.ServletProxy;

/**
 * This servlet is for deleting 3D objects from the database via a HTML form.
 * 
 * @author Daniel Pawsey
 * @since 23/07/2015
 */
@WebServlet(name = "DeleteObjectServlet", urlPatterns = {"/DeleteObjectServlet"})
public class DeleteObjectServlet extends HttpServlet {

    private ServletProxy proxy;
    private DatabaseWriter dbWriter;
    private String type;
    private String object;

    public DeleteObjectServlet() {
        proxy = new ServletProxy();
        dbWriter = new DatabaseWriter();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Administrator area | Data Centre Modelling</title>");
            out.println("<meta charset=\"UTF-8\">");
            out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\">");
            out.println("<script src=\"http://code.jquery.com/jquery-1.9.1.js\"></script>");
            out.println("<script src=\"js/utils/PageUtils.js\"></script>");
            out.println("</head>");
            out.println("<body onload=\"$('#backendContent').empty().load('BackendPage.html #pageContentArea');\">");
            out.println("<div class=\"successMessage\">");
            out.println(type + " " + object + " deleted.");
            out.println("</div>");
            out.println("<div id=\"backendContent\"><p>This content should be deleted...</p></div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * 
     * Deletes the selected object from the database, and regenerates the model
     * to reflect the change.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        type = request.getParameter("type");
        object = request.getParameter("object");

        dbWriter.deleteRecord(type, object);
        proxy.regenerateModel();
        
        // The model is regenerated after the deletion from the table, so that
        // the change to the database is immediatly  reflected in the model.
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
