package ServletPackage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import mvc.controller.DatabaseTools.DatabaseWriter;
import mvc.controller.ServletProxy;

/**
 * This servlet is for adding 3D objects to the database.
 * 
 * @author Daniel Pawsey
 * @since 22/07/2015
 */
@WebServlet(name = "AddObjectServlet", urlPatterns = {"/AddObjectServlet"})
@MultipartConfig
public class AddObjectServlet extends HttpServlet {

    private String query;
    private String type;
    private String name;
    private String kWh;
    private String co2;
    private String temp;
    private String tempThreshold;
    private String modelName;
    private String modelContent;
    private Part filePart;
    private ServletProxy proxy;
    private DatabaseWriter dbWriter;

    public AddObjectServlet() {
        proxy = new ServletProxy();
        dbWriter = new DatabaseWriter();
        type = "";
        name = "";
        kWh = "";
        co2 = "";
        temp = "";
        tempThreshold = "";
        modelName = "";
        modelContent = "";
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @param validUpload a flag for whether or not the upload was successful
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(boolean validUpload, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Add or delete objects | Data Centre Modelling</title>");
            out.println("<meta charset=\"UTF-8\">");
            out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            out.println("<script src=\"http://code.jquery.com/jquery-1.9.1.js\"></script>");
            out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\">");
            out.println("<script src=\"js/utils/PageUtils.js\"></script>");

            if (validUpload) {
                out.println("<body onload=\"$('#backendContent').empty().load('BackendPage.html #pageContentArea');\">");
                out.println("<div class=\"successMessage\">");
                out.println(type + " " + name + " Added.");
                out.println("</div>");
                out.println("<div id=\"backendContent\"><p>This content should be deleted...</p></div>");
            } else {
                out.println("<script src=\"http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js\"></script>");
                out.println("</head>");
                out.println("<body onload=\"loadForm('AddObjectForm')\" >");
                out.println("<div class=\"failMessage\">");
                out.println("<p>The upload failed. Please ensure that you fill out all fields.</p>");
                out.println("</div>");
                out.println("<div id=\"pageContent\">");
                out.println("<div id=\"pageContentArea\"></div>");
            }
            out.println("<div id=\"pageLinks\">");
            out.println("<a href=\"BackendPage.html\">Add or delete another object</a>");
            out.println("<a href=\"index.html\">Back to the app</a>");
            out.println("</div>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(true, request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * Gets the attributes from the HttpServletRequest, passes them to an
     * ArrayList of Strings, and passes that ArrayList to
     * {@link mvc.controller.DatabaseTools.DatabaseWriter} in order to write the
     * form details from AddObjectForm.jsp to the database table defined by the
     * "type" attribute entered into the form.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {

            type = request.getParameter("type");

            ArrayList<String> attributesList = new ArrayList<String>();

            name = request.getParameter("name");
            attributesList.add(name);

            kWh = request.getParameter("kWh");
            attributesList.add(kWh);

            co2 = request.getParameter("co2");
            attributesList.add(co2);

            temp = request.getParameter("temp");
            attributesList.add(temp);

            tempThreshold = request.getParameter("tempThreshold");
            attributesList.add(tempThreshold);

            modelName = request.getParameter("jsonModel");

            filePart = request.getPart("jsonModel");

            modelName = getFileName(filePart);

            modelContent = getFileContent(filePart);
            attributesList.add(modelContent);

            dbWriter.writeRecord(type, attributesList);

            boolean validUpload = false;

            // Check that all fields are filled out before attempting to submit 
            // to the database
            if (!type.isEmpty() && !name.isEmpty() && !kWh.isEmpty() && !co2.isEmpty() && !temp.isEmpty() && !tempThreshold.isEmpty() && !modelName.isEmpty() && !modelContent.isEmpty()) {
                validUpload = true;
                query = dbWriter.getQuery();
                // Regenerate the model so that it includes the newly added 
                // object.
                proxy.regenerateModel();
            } else {
                validUpload = false;
            }

            // Generate output based on whether or not the upload succeeded
            processRequest(validUpload, request, response);

        } catch (IOException | ServletException ex) {
            Logger.getLogger(AddObjectServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Extracts and returns the content of an uploaded file.
     *
     * @see
     * ServletPackage.AddObjectServlet#doPost(javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     *
     * @param filePart The {@link javax.servlet.http.Part} from which the
     * content fileContentStream extracted.
     *
     * @return The content of the file
     *
     * @throws IOException if an I/O error occurs
     */
    private static String getFileContent(Part filePart) throws IOException {

        InputStream fileContentStream = filePart.getInputStream();
        BufferedReader bufferedReader = null;
        StringBuilder stringBuilder = new StringBuilder();

        String contentLine;
        try {

            bufferedReader = new BufferedReader(new InputStreamReader(fileContentStream));
            while ((contentLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(contentLine);
            }
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }

        return stringBuilder.toString();

    }

    /**
     * Get the name from a Part (supplied file)
     * 
     * @param part The supplied file.
     * 
     * @return The name of the supplied Part.
     */
    private static String getFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
