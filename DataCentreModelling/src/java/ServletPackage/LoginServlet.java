package ServletPackage;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.CredentialChecker;

/**
 * This servlet is called by the form on LoginPage.html, checking the supplied
 * username and password using an object of class
 * {@link utils.CredentialChecker} to check whether the supplied values match
 * any records in the database. If so, it will redirect the user to
 * BackendPage.html, if not, it will generate a new login page with an error
 * message in the HTML.
 *
 * @author Daniel Pawsey
 * @since 23/07/2015
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    private String username;
    private String password;
    private CredentialChecker credentialChecker;

    public LoginServlet() {
        credentialChecker = new CredentialChecker();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response, boolean goodCredentials)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            if (goodCredentials) {
                response.sendRedirect("BackendPage.html");
            } else {
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Incorrect username or password | Data Centre Modelling</title>");
                out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\">");
                out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/LoginPage.css\">");
                out.println("<script src=\"http://code.jquery.com/jquery-1.9.1.js\"></script>");
                out.println("<script src=\"http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js\"></script>");
                out.println("</head>");
                out.println("<body onload=\"loadLoginPage()\">");
                out.println("<div class=\"pageCard\" id=\"loginForm\">");
                out.println("<form id=\"theForm\" action=\"LoginServlet\" method=\"GET\" ng-app>");
                out.println("<h1>Enter your sign-in details</h1>");
                out.println("Username: <input class=\"loginText\" ");
                out.println("type=\"text\" ");
                out.println("name=\"username\" ");
                out.println("ng-model=\"fieldCheck.username\">");
                out.println("<br>");
                out.println("Password: <input class=\"loginText\" ");
                out.println("type=\"password\" ");
                out.println("name=\"password\" ");
                out.println("ng-model=\"fieldCheck.password\">");
                out.println("<br>");
                out.println("<div class=\"formButtons\">");
                out.println("<input type=\"submit\"");
                out.println("class=\"controlButton\"");
                out.println("value=\"Sign in\"");
                out.println("ng-disabled=\"!(!!fieldCheck.username && !!fieldCheck.password)\"/>");
                out.println("<input class=\"controlButton\" ");
                out.println("id=\"cancelButton\" type=\"reset\" ");
                out.println("value=\"Cancel\" ");
                out.println("onclick=\"clearCentreBox()\"/>");
                out.println("<a href=\"index.html\">Back to the app</a>");
                out.println("</div>");
                out.println("</form>");
                out.println("<div class=\"failMessage\">");
                out.println("<p>Incorrect username or password.</p>");
                out.println("</div");
                out.println("</div>");
                out.println("</body>");
                out.println("</html>");
            }

        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * <p>
     * Handles the HTTP <code>GET</code> method.</p>
     *
     * <p>
     * Checks the supplied user credentials using 
     * {@link utils.CredentialChecker#checkCredentials(java.lang.String, java.lang.String)}.
     * </p>
     *
     * @see utils.CredentialChecker
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        username = request.getParameter("username");
        password = request.getParameter("password");

        boolean goodCredentials = credentialChecker.checkCredentials(username, password);

        processRequest(request, response, goodCredentials);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, false);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
