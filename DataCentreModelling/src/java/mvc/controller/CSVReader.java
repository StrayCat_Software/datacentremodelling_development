package mvc.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to read CSV files and return their lines as rows.
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 29/05/2015
 * 
 * @deprecated Use {@link mvc.controller.DatabaseTools.DatabaseReader} instead.
 */
public class CSVReader {
    
    public String log = "";

    /**
     * This method reads through all of the rows of a parameterised CSV file,
     * and adds each one as a {@link java.lang.String} to a
     * {@link java.util.ArrayList}.
     *
     * @param filePath The file-path of the CSV file to be read.
     *
     * @return An ArrayList of file names as Strings.
     *
     * @deprecated Not able to retrieve data from a URL. Retained for testing
     * purposes. Use
     * {@link mvc.controller.CSVReader#retrieveRowsFromUrl(java.lang.String)}
     * instead.
     */
    public ArrayList<String> retrieveRows(String filePath) {

        //System.out.println("CSVReader.retrieveRows called with parameter \"" + filePath + "\".");
        BufferedReader reader = null;
        ArrayList<String> fileNameList = new ArrayList();

        try {
            //First read the specified file to determine the names of files to be read.
            reader = new BufferedReader(new FileReader(filePath));
            String record = null;

            // Add each row from the specified file to fileNameList.
            while ((record = reader.readLine()) != null) {
                fileNameList.add(record);
                //System.out.println("CSVReader.retrieveRows: " + record);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CSVReader.class.getName()).log(Level.SEVERE, null, ex);
            //System.out.println(filePath + " called, but not found.");
        } catch (IOException ex) {
            Logger.getLogger(CSVReader.class.getName()).log(Level.SEVERE, null, ex);
        }

        return fileNameList;
    }

    /**
     * * This method reads through all of the rows of a parameterised CSV file
     * from a URL, and adds each one as a {@link java.lang.String} to a
     * {@link java.util.ArrayList}.
     *
     * @see mvc.controller.CSVReader#retrieveRows(java.lang.String)
     *
     * @param urlPath The file-path of the CSV file to be read.
     *
     * @return An ArrayList of file names as Strings.
     */
    public ArrayList<String> retrieveRowsFromUrl(String urlPath) {
        
        ArrayList<String> fileNameList = new ArrayList<String>();
        try {
        //First read the specified URL to determine the names of files to be read.
            System.out.println("parsing " + urlPath + " as a URL");
            log += "CSVReader#retrieveRowsFromURL("+urlPath+"): parsing " + urlPath + " as a URL";
        URL csvFileUrl = new URL(urlPath);
        BufferedReader reader = new BufferedReader(
        new InputStreamReader(csvFileUrl.openStream()));
        
        // Add each row from the specified file to fileNameList.
        String record;
        
            while((record = reader.readLine()) != null){
                fileNameList.add(record);
            }
        reader.close();
        
        } catch (IOException ex) {
            Logger.getLogger(CSVReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return fileNameList;
    }

    /**
     * Reads a single CSV format row, parsing it's attributes as Strings within
     * an ArrayList.
     *
     * @param record The CSV format row to be parsed.
     *
     * @return The CSV format row parsed into an {@link java.util.ArrayList} of
     * {@link java.lang.String Strings}.
     */
    public ArrayList<String> readRecord(String record) {

        //System.out.println("CSVReader.readRecord(" + record + ") called.");
        ArrayList<String> recordAttributeStringList = new ArrayList<>();

        String recordAttributeString = "";

        // convert the record to a char array
        char[] recordCharArray = record.toCharArray();

        // Iterate through the char array, concatenating each charicter that is
        // not a comma into rowValue, and add that string 
        // to recordAttributeStringList every time a comma is encountered
        for (int i = 0; i <= recordCharArray.length - 1; i++) {
            //As long as the current charater in the character array is not a 
            //comma, concatenate it into the string
            if (recordCharArray[i] != ',') {
                recordAttributeString += recordCharArray[i];
            }

            // If the current character is a comma, add recordAttributeStringList
            // to the ArrayList, then empty the string.
            if (recordCharArray[i] == ',') {
                recordAttributeStringList.add(recordAttributeString);
                //System.out.println("added " + recordAttributeString + " to recordAttributeStringList");
                recordAttributeString = "";
            }
        }

        return recordAttributeStringList;
    }
    
    public String getLog(){
        return log;
    }
}
