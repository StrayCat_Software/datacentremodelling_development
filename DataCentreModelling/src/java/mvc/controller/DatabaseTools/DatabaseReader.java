package mvc.controller.DatabaseTools;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mvc.controller.modelobject.ModelObjectFactory;
import mvc.testharness.JSONFromDatabaseHarness;

/**
 *
 * @author Daniel Pawsey
 * @since 22/07/2015
 */
public class DatabaseReader {

    private DatabaseConnection dbAccessor;
    private ArrayList<String> tableList;

    public DatabaseReader() {
        dbAccessor = new DatabaseConnection();
        tableList = dbAccessor.getTables();
    }
    
    public ArrayList<String> getTableList(){
        System.out.println("DatabaseReader.getTableList() called.");
        return tableList;
    }

    public static DatabaseReader getInstance() {
        return DatabaseReaderHolder.INSTANCE;
    }

    private static class DatabaseReaderHolder {

        private static final DatabaseReader INSTANCE = new DatabaseReader();
    }
    
    /**
     * 
     * @param tableName The name of the table to be read for objects to be constructed
     */
    public void readTable(ModelObjectFactory modelObjectFactory, String tableName) {
        
        System.out.println("DatabaseReader.readTable query: " + dbAccessor.getQuery());
        
        try {
            DatabaseConnection dba = new DatabaseConnection();
            String query = "SELECT * FROM " + tableName + ";";
            ResultSet rs = dba.query(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int attributeCount = rsmd.getColumnCount();
            
            while (rs.next()) {
                
                ArrayList<String> attributeList = new ArrayList<String>();
                
                for (int i = 1; i <= attributeCount; i++) {
                    System.out.println(rs.getString(i));
                    attributeList.add(rs.getString(i));
                }
                modelObjectFactory.constructObject(attributeList, tableName);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(JSONFromDatabaseHarness.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
