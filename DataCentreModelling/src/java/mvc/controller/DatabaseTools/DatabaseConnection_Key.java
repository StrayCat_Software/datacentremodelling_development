package mvc.controller.DatabaseTools;

/**
 *
 * @author Daniel Pawsey
 */
public class DatabaseConnection_Key {

    private final String connectionDriver = "jdbc";
    private final String databaseType = "postgresql";
    private final String hostName = "cmp-14javapro01.cmp.uea.ac.uk";
    private final String databaseName = "wxu13keu";

    /**
     * the user name for the database.
     */
    private final String databaseUser = "wxu13keu";

    /**
     * The password for the database.
     */
    private final String databasePassword = "wxu13keu";

    /**
     * @return a concatenated database URL, consisting of the database type,
     * host and name attributes, forming a string like
     * "jdbc:postgresql:https://cmp-14javapro01.cmp.uea.ac.uk/wxu13keu".
     */
    protected String getURL() {
        return connectionDriver
                + ":"
                + databaseType
                + "://"
                + hostName
                + "/"
                + databaseName;
    }

    /**
     * @return
     * {@link mvc.controller.DatabaseTools.DatabaseConnection_Key#databaseUser}
     */
    protected String getUser() {
        return databaseUser;
    }

    /**
     * @return
     * {@link mvc.controller.DatabaseTools.DatabaseConnection_Key#databasePassword}
     */
    protected String getPassword() {
        return databasePassword;
    }
}
