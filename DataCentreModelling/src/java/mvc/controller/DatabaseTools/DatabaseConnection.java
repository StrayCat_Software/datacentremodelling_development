/*
 As database connections and statements are regularly made within this web app,
 this class is designed to encapsulate the JDBC connection-statement-close
 process, so that an SQL update or query can be performed using a single
 statement, with the SQL command supplied as a parameter to it.
 */
package mvc.controller.DatabaseTools;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mvc.controller.CSVReader;
import utils.SiteUtils;

/**
 *
 * @author Daniel Pawsey
 * @since 22/07/2015
 */
public class DatabaseConnection {

    private String databaseURL;
    private String databaseUser;
    private String databasePassword;
    private String sqlStatement;
    private Connection conn = null;
    private Statement stmt = null;
    private ResultSet rs = null;
    private CSVReader csvReader;
    private SiteUtils siteUtils;

    /**
     *
     */
    public DatabaseConnection() {
        DatabaseConnection_Key dbkey = new DatabaseConnection_Key();
        databaseURL = dbkey.getURL();
        databaseUser = dbkey.getUser();
        databasePassword = dbkey.getPassword();
        System.out.println("DBAccessor constructed");
        sqlStatement = "No SQL statement made.";
    }

    public DatabaseConnection(String siteUrl) {
        csvReader = new CSVReader();
        generateAccessKey(siteUrl);
        System.out.println("DBAccessor constructed");
    }

    /**
     *
     */
    private void generateAccessKey(String siteUrl) {
        String url = siteUtils.getURL(null);
        String filePath = url + "resources/connection/credentials.csv";
        ArrayList<String> accessCredentials = csvReader.retrieveRowsFromUrl(filePath);
    }

    /**
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void connect() throws SQLException, ClassNotFoundException {

        //Connect to the databse
        Class.forName("org.postgresql.Driver");
        conn = DriverManager.getConnection(databaseURL, databaseUser, databasePassword);

        //Create the statement
        stmt = conn.createStatement();
    }

    /*
     This is called at the end of both the update and query functions
     */
    public void disconnect() throws SQLException, ClassNotFoundException {
        //rs.close();
        //stmt.close();
        conn.close();
    }

    /*
     Update statement method
     */
    public void update(String sqlStatement) throws SQLException {
        this.sqlStatement = sqlStatement;

        try {

            if (sqlStatement.equals("CANCEL")) {
            } else {
                //Connect to the database, and create the statement
                this.connect();

                //Execute the update
                stmt.executeUpdate(sqlStatement);

                //Disconnect from the database
                this.disconnect();
            }
        } catch (SQLException | ClassNotFoundException | NullPointerException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Statement getStatement() throws SQLException, ClassNotFoundException {
        //Connect to the database, and create the statement
        this.connect();
        return stmt;
    }

    public ResultSet query(String sqlStatement) throws SQLException, ClassNotFoundException {
        this.sqlStatement = sqlStatement;
        this.connect();
        rs = stmt.executeQuery(sqlStatement);
        this.disconnect();
        return rs;
    }

    /**
     * @return The names of the tables in the connected database.
     */
    public ArrayList<String> getTables() {

        ArrayList<String> tablesList = new ArrayList<String>();

        try {
            connect();
            DatabaseMetaData dbmd = conn.getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = dbmd.getTables(null, null, "%", types);
            while (rs.next()) {
//                System.out.println(rs.getString("TABLE_NAME"));
                tablesList.add(rs.getString("TABLE_NAME"));
            }
            disconnect();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tablesList;
    }
    
    /**
     * @return The most recently used SQL statement.
     */
    public String getQuery(){
        return sqlStatement;
    }
}
