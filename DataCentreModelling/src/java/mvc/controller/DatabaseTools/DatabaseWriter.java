/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.controller.DatabaseTools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Pawsey
 * @since 23/07/2015
 */
public class DatabaseWriter {

    private final DatabaseConnection dbAccessor;
    private String query;

    public DatabaseWriter() {
        dbAccessor = new DatabaseConnection();
        query = "";
    }

    /**
     * Converts the record to be added to the database table into a single
     * String formatted for an SQL insert or update statement.
     *
     * @param recordAttributes The ArrayList of values to be converted to a
     * String.
     *
     * @return An SQL formatted String of values for a record to be added to a
     * database table.
     */
    private String StringifyRecord(ArrayList<String> recordAttributes) {

        String attributesAsString = "";
        for (int i = 0; i <= recordAttributes.size() - 1; i++) {
            attributesAsString += "'" + recordAttributes.get(i) + "'";

            // Add a comma if the current attribute is not the last one in 
            // the list.
            if (i < recordAttributes.size() - 1) {
                attributesAsString += ",";
            }
        }

        return attributesAsString;
    }

    /**
     * Writes an SQL formatted record to a supplied database table.
     *
     * @param table The name of the table to which the new record will be added.
     *
     * @see
     * mvc.controller.DatabaseTools.DatabaseWriter#StringifyRecord(java.util.ArrayList)
     *
     * @param recordAttributes A record to be added to a supplied database table
     * preformatted as an SQL formatted String.
     */
    public void writeRecord(String table, ArrayList<String> recordAttributes) {
        try {
            String recordString = StringifyRecord(recordAttributes);
            System.out.println("INSERT INTO " + table + " VALUES(" + recordString + ");");
            dbAccessor.update("INSERT INTO " + table + " VALUES(" + recordString + ");");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Removes a record from a specified table, identified by a supplied
     * primary-key value.
     *
     * @param table The table to delete the record from.
     *
     * @param pKeyAttribute The primary-key value of the record to be deleted.
     */
    public void deleteRecord(String table, String pKeyAttribute) {
        try {
            dbAccessor.update("DELETE FROM " + table + " WHERE name = '" + pKeyAttribute + "'");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns the last value of this class's
     * {@link mvc.controller.DatabaseTools.DatabaseWriter#query} field. Used to
     * debug SQL statement formation.
     *
     * @return The most recent value of
     * {@link mvc.controller.DatabaseTools.DatabaseWriter#query}.
     */
    public String getQuery() {
        query = dbAccessor.getQuery();
        return query;
    }
}
