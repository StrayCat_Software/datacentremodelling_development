package mvc.controller;

/**
 * <p>
 * This class is used to store and provide the rool URL at which the site is
 * being hosted. It is stored as a class because it is possible for multiple
 * Java classes to require this data.</p>
 *
 * <p>
 * This also serves as a convienient place from which the root URL for the
 * site's resources can be changed when migrating from a locally hosted testing
 * environment to a live server.</p>
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 03/06/2015
 */
public class URLProvider {

    /**
     * <p>
     * This {@link java.lang.String} is initialised by the
     * {@link mvc.controller.URLProvider#URLProvider() constructor} of this
     * class.</p>
     *
     * <p>
     * This {@link java.lang.String} will include the dividing "/" slash between
     * the URL and the path to the resource which follows it, as it can be used
     * as the root url in itself, but the resource path extention may, in some
     * cases be unreadable when beginning with a "/" slash. As such,
     * <code>https://www.mysite.com/</code> is be an example of a value to
     * assign to this {@link java.lang.String}.
     * </p>
     */
    private String siteUrl;

    /**
     * This constructor initialises {@link mvc.controller.URLProvider#siteUrl}.
     */
    public URLProvider() {
        siteUrl = "http://localhost:8080/";
    }

    /**
     * Return the URL of the site calling this method.
     * 
     * @return {@link mvc.controller.URLProvider#siteUrl}
     */
    public String getSiteUrl() {
        return siteUrl;
    }

}
