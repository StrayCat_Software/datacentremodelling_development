package mvc.controller.modelobject;

import java.util.ArrayList;
import mvc.model.ModelObjectInterface;

/**
 * This singleton class serves as static storage for objects of classes which
 * implement {@link mvc.model.ModelObjectInterface}.
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 15/05/2015
 */
public class ModelObjectPool {

    /**
     * A list of objects which implement the
     * {@link mvc.controller.ModelObject ModelObjectInterface} interface.
     */
    private ArrayList<ModelObjectInterface> modelObjectList;

    /**
     * A static object of
     * {@link mvc.controller.mvc.modelobject.ModelObjectPool this class} which
     * is instantiated on construction in order to implement this class as a
     * singleton.
     */
    private static volatile ModelObjectPool instance = new ModelObjectPool();

    /**
     * This classes constructor is private, as it is a singleton and thus is
     * statically constructed from within on the system loading.
     */
    private ModelObjectPool() {
        modelObjectList = new ArrayList<ModelObjectInterface>();
    }

    /*
     * Instantiation method for this singleton class.
     */
    public static ModelObjectPool getInstance() {
        return instance;
    }

    /**
     * Adds a {@link mvc.model.ModelObjectInterface ModelObjectInterface}
     * implementing object
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList}.
     *
     * @param modelObject The
     * {@link mvc.model.ModelObjectInterface ModelObjectInterface} to be added
     * to the
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList}.
     */
    public void addModelObject(ModelObjectInterface modelObject) {
        
        modelObjectList.add(modelObject);
        System.out.println("Added " + modelObject.getClass() + " to ModelObjectPool");
    }
    
    /**
     * Returns a ModelObjectInterface implementing object from the
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList}
     * ArrayList.
     *
     * @param index The index of the ModelObjectInterface implementing object to
     * be retrieved from the
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList mvc.modelObjectList}
     * ArrayList.
     *
     * @return A mvc.model package object which implements the
     * {@link mvc.model.ModelObjectInterface ModelObjectInterface} interface.
     */
    public ModelObjectInterface getModelObjectbyIndex(int index) {
        return modelObjectList.get(index);
    }

    /**
     * Used to retrieve the whole
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList mvc.modelObjectList}
     * ArrayList.
     *
     * @return An ArrayList of objects of any class which which implements the
     * {@link mvc.model.ModelObjectInterface ModelObjectInterface} interface.
     */
    public ArrayList<ModelObjectInterface> getModelObjectList() {
        return modelObjectList;
    }

    /**
     * Allows objects of a certain type to be retrieved from
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList mvc.modelObjectList}.
     * These are returned to the calling object as an ArrayList, which it can
     * process itself.
     *
     * @param type The class name of the objects to be returned by this method.
     *
     * @return An ArrayList of objects which implement the ModelObjectInterface
     * interface. These have been found to return a class name which matches the
     * parameter String of this method, which is retrieved by calling the
     * returned object's individual getModelObjectType methods.
     */
    public ArrayList<ModelObjectInterface> getModelObjectsByType(String type) {
        ArrayList<ModelObjectInterface> filteredList = new ArrayList();

        for (ModelObjectInterface modelObjectInterface : modelObjectList) {
//            if (modelObjectInterface.getClass().getSimpleName().equals(type)) {
            if (modelObjectInterface.getName().equals(type)) {
                filteredList.add(modelObjectInterface);
            }
        }

        return filteredList;
    }

    /**
     * Filters
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList} by
     * element superclass. For example, any class
     * {@link mvc.model.server.ConcreteServer} objects in
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList} can be
     * returned by passing the class name of
     * {@link mvc.model.server.superclass.Server} as a String.
     *
     * @param superClassName the superclass name to filter
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList mvc.modelObjectList}
     * by.
     * @return An ArrayList of all of the objects of the class specified by the
     * superClassName parameter.
     */
    public ArrayList<ModelObjectInterface> getModelObjectsBySuperClass(String superClassName)
            throws IndexOutOfBoundsException {
        ArrayList<ModelObjectInterface> filteredList = new ArrayList();

        for (ModelObjectInterface modelObjectInterface : modelObjectList) {
            if (modelObjectInterface.getSuperclassName().equals(superClassName)) {
                filteredList.add(modelObjectInterface);
            }
        }
        return filteredList;
    }

    /**
     * Delete all objects currently contained within
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList}.
     */
    public void clearModelObjectList() {
        modelObjectList.clear();
    }

}
