package mvc.controller.modelobject;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mvc.controller.CSVReader;
import mvc.controller.DatabaseTools.DatabaseReader;
import mvc.model.ModelObjectInterface;
import utils.ControllerUtils;

/**
 * @author Daniel Pawsey
 * @version 2
 * @since 15/05/2015
 */
public class ModelObjectFactory {

    private CSVReader csvReader;
    private ModelObjectPool thePool;
    private DatabaseReader databaseReader;
    private ControllerUtils utils;
    private String log = "";

    public ModelObjectFactory() {
        csvReader = new CSVReader();
        thePool = ModelObjectPool.getInstance();
        databaseReader = DatabaseReader.getInstance();
        utils = new ControllerUtils();
    }

    /**
     * <p>
     * Get the tables names from the database, and use every one except for the
     * administrator table to call
     * {@link mvc.controller.DatabaseTools.DatabaseReader#readTable(mvc.controller.modelobject.ModelObjectFactory, java.lang.String)}.</p>
     *
     * <p>
     * {@link mvc.controller.DatabaseTools.DatabaseReader#readTable(mvc.controller.modelobject.ModelObjectFactory, java.lang.String)}
     * calls
     * {@link mvc.controller.modelobject.ModelObjectFactory#constructObject(java.util.ArrayList, java.lang.String)}
     * for every record it finds.</p>
     *
     * @see mvc.controller.modelobject.ModelObjectPool#clearModelObjectList()
     * @see mvc.controller.DatabaseTools.DatabaseReader#getTableList()
     */
    public void execute() {
        // This first clears the pool, so that duplicates are not added if 
        // execute is called again.
        thePool.clearModelObjectList();
        System.out.println("ModelObjectFactory.execute() called.");
        ArrayList<String> tablesList = databaseReader.getTableList();

        for (int i = 0; i <= tablesList.size() - 1; i++) {
            System.out.println("querying with table " + tablesList.get(i));
            if (!tablesList.get(i).equals("administrator")) {
                databaseReader.readTable(this, tablesList.get(i));
            }
        }
    }

    /**
     * <p>
     * This method uses attributes filtered from a record of one of the object
     * super-type data files, and uses it to construct a model package object
     * which implements {@link mvc.model.ModelObjectInterface}: The exact class
     * of those objects is determined by data provided as a parameter to this
     * method.</p>
     *
     * <p>
     * The attributes read from the database table are interpreted by this method as
     * follows:
     * </p>
     *
     * <ol>
     * <li>The name of the class to construct.</li>
     * <li>The kWh attribute of the class to construct.</li>
     * <li>The co2 attribute of the class to construct.</li>
     * </ol>
     *
     * <p>
     * This method is called by
     * {@link mvc.controller.modelobject.ModelObjectFactory#execute()}.</p>
     *
     * @version 2.0
     * @since 11/06/2015
     */
    public void constructObject(ArrayList recordAttributeStringList, String table) {

        try {

            String modelObjectType = "mvc.model." + table + "." + "Concrete" + utils.capitaliseFirstLetter(table);

            // The first attribute of any record should be the name attribute of the object.
            String name = recordAttributeStringList.get(0).toString();

            // The class is created, and it's type is assigned retrospectively
            // using reflection via the Class.forName(String) method, and then
            // by casting a new instance of that class to a ModelObjectInterface.
            Class modelObjectClass = Class.forName(modelObjectType);
            ModelObjectInterface modelObject = (ModelObjectInterface) modelObjectClass.newInstance();

            modelObject.setSuperclass(utils.capitaliseFirstLetter(table));

            // Set the name of modelObject.
            if (recordAttributeStringList.size() >= 1) {
                modelObject.setName(name);
                System.out.println("modelObject NAMED " + modelObject.getName());
            }
            // Set the kWh of modelObject.
            if (recordAttributeStringList.size() >= 2) {
                modelObject.setKWh(Double.parseDouble(recordAttributeStringList.get(1).toString()));
                System.out.println("modelObject KWH = " + modelObject.getKWh());
            }
            // Set the co2 of modelObject
            if (recordAttributeStringList.size() >= 3) {
                modelObject.setCo2(Double.parseDouble(recordAttributeStringList.get(2).toString()));
                System.out.println("modelObject CO2 = " + modelObject.getCo2());
            }
            // Set the temp of modelObject
            if (recordAttributeStringList.size() >= 3) {
                modelObject.setTemp(Double.parseDouble(recordAttributeStringList.get(3).toString()));
                System.out.println("modelObject TEMP = " + modelObject.getTemp());
            }
            // Set the tempThreshold of modelObject
            if (recordAttributeStringList.size() >= 4) {
                modelObject.setTempThreshold(Double.parseDouble(recordAttributeStringList.get(4).toString()));
                System.out.println("modelObject TEMPTHRESHOLD = " + modelObject.getTempThreshold());
            }
            // Get the JSON model for the modelObject
            // Assign the JSON data to the object
            if (recordAttributeStringList.size() >= 5) {
                modelObject.setJson_data(recordAttributeStringList.get(5).toString());
                System.out.println("modelObject JSON DATA = " + modelObject.getJson_data());
            }

            // Iterate through all objects currently in ModelObjectPool: If the
            // class currently being added is already, it will not be added,
            // otherwise it will be.
            boolean addModelObjectToPool = true;

            // A check is made whether an object of modelObject's name is 
            // already in the pool before it is added.
            for (ModelObjectInterface checkModelObject : thePool.getModelObjectList()) {
                if (checkModelObject.getName().equals(modelObject.getName())) {
                    addModelObjectToPool = false;
                }
            }

            // If an object of class ModelObject is not already in the pool, add
            // it.
            if (addModelObjectToPool == true) {
                thePool.addModelObject(modelObject);
                System.out.println("ModelObjectFactory: " + modelObject.getClass().getSimpleName() + " named " + modelObject.getName() + " added to the pool");
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ModelObjectFactory.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Get a an ArrayList of the names of all of the tables (which are not the
     * administrator table) as Strings.
     *
     * @return The names of all of the tables (which are not the administrator
     * table) as an ArrayList of Strings.
     */
    public ArrayList<String> getTableList() {
        return databaseReader.getTableList();
    }

}
