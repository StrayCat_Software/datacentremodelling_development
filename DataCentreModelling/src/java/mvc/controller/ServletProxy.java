package mvc.controller;

import java.util.ArrayList;
import mvc.controller.modelobject.ModelObjectFactory;
import mvc.controller.modelobject.ModelObjectPool;
import mvc.model.ModelObjectInterface;
import utils.ControllerUtils;

/**
 * <p>
 * This class aggregates all data from other controller classes to allow for
 * controlled passage of data from the controller layer to a servlet.</p>
 *
 * <p>
 * This confers the benefit of allowing the servlet to be of a different
 * language to the model and controller, or not to be a servlet at all, as may
 * be the case if AJAX or jQuery are used.</p>
 *
 * @see mvc.controller.modelobject.ModelObjectFactory
 * @see mvc.controller.modelobject.ModelObjectPool
 * @see mvc.model.ModelObjectInterface
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 27/05/2015
 */
public class ServletProxy {

    private final ModelObjectFactory modelObjectFactory;
    private final ModelObjectPool modelObjectPool;
    private final ControllerUtils controllerUtils;
    ArrayList<String> tableList;
    ArrayList<String> filteredTableList;
    private String log = "";

    /**
     * <p>
     * This constructor does the following:</p>
     * <ul>
     * <li>It initialises and runs a
     * {@link mvc.controller.modelobject.ModelObjectFactory} as
     * {@link mvc.controller.ServletProxy#modelObjectFactory}, which ensures
     * that the model is generated.</li>
     * <li>It gets an instance of the singleton class
     * {@link mvc.controller.modelobject.ModelObjectPool} as
     * {@link mvc.controller.ServletProxy#modelObjectPool}, allowing the model
     * to be accessed.</li>
     * <li>It constructs a {@link utils.ControllerUtils} as
     * {@link mvc.controller.ServletProxy#controllerUtils}.</li>
     * <li>It calls
     * {@link mvc.controller.ServletProxy#populateTableList()}.</li>
     * </ul>
     */
    public ServletProxy() {
        modelObjectFactory = new ModelObjectFactory();
        modelObjectFactory.execute();
        modelObjectPool = ModelObjectPool.getInstance();
        controllerUtils = new ControllerUtils();
        populateTableList();
    }

    /**
     * populates {@link mvc.controller.ServletProxy#filteredTableList} with the
     * names of all of the tables in the database except for the administrator
     * table.
     *
     * @see utils.ControllerUtils#filterOut(java.util.ArrayList,
     * java.lang.String)
     */
    private void populateTableList() {
        tableList = modelObjectFactory.getTableList();
        filteredTableList = controllerUtils.filterOut(tableList, "administrator");
    }

    /**
     * This method returns all concrete model objects formatted as HTML buttons
     * as a single String. It calls them using
     * {@link mvc.controller.modelobject.ModelObjectPool#getModelObjectsBySuperClass(java.lang.String)},
     * which is supplied super-class names by
     * {@link mvc.controller.modelobject.ModelObjectFactory#getTableList()}.
     *
     * @param superClass The super-class name of the classes to be formatted as
     * HTML buttons.
     *
     * @return All of the URIs formatted as HTML buttons, as a single string.
     *
     */
    private String formatJsonUrisOfTypeAsButtons(String superClass) {
        String logNote = "ServletProxy#formatJsonUrisOfTypeAsButtons(" + superClass + ") called.";
        System.out.println(logNote);
        log += logNote;
        // The buttons are wrapped in a div with the id of the supertype name 
        // + buttons, e.g. "<div id="ServerButtons>".

        String htmlButtons = "<div id=\"" + superClass + "Buttons\">";
        ArrayList<ModelObjectInterface> modelObjectList = modelObjectPool.getModelObjectsBySuperClass(superClass);
        for (ModelObjectInterface modelObject : modelObjectList) {
            htmlButtons += "<button class=\"objectButton\" onclick=\"setModel('" + modelObject.getName() + "')\">" + modelObject.getName() + "</button>\n";
        }

        htmlButtons += "</div>";
        return htmlButtons;
    }

    /**
     * Debugging method.
     *
     * @return {@link mvc.controller.ServletProxy#log}
     */
    public String getLog() {
        return log;
    }

    /**
     * Retrieves a specific attribute from each of the
     * {@link mvc.model.ModelObjectInterface} implementing objects and formats
     * them all into HTML tags identifiable by their class name plus a FIRST
     * LETTER CAPITALISED version of their name, for example,
     * {@link mvc.model.server.ConcreteServer}'s
     * {@link mvc.model.AbstractModelObject#co2} value would be formatted as
     * <code>ConcreteServerCo2</code>, as the value of an <code>id</code>
     * attribute of a <code>div</code> tag.
     *
     * @param superClass The super-class of all of the
     * {@link mvc.model.ModelObjectInterface} implementing objects to be
     * included in the attribute recovery and formatting process.
     *
     * @param metricType The specific attribute of each
     * {@link mvc.model.ModelObjectInterface} implementing object to be
     * included. This should be supplied with a capitalised first letter,
     * otherwise {@link mvc.controller.ServletProxy#capitaliseFirstLetter} will
     * be called on that parameter.
     *
     * @return The requested attribute for each
     * {@link mvc.model.ModelObjectInterface} implementing object, formatted
     * into HTML <code>div</code> tags bearing the class name and attribute name
     * of each object.
     *
     * @see utils.ControllerUtils#capitaliseFirstLetter(java.lang.String)
     * @see
     * mvc.controller.modelobject.ModelObjectPool#getModelObjectsBySuperClass(java.lang.String)
     * @see mvc.model.AbstractModelObject#getMetric(java.lang.String)
     */
    private String getObjectMetricsBySuperclass(String superClass, String metricType) {

        // Logging
        String logNote = "ServletProxy#getObjectMetrics(" + superClass + ", " + metricType + ") called.";
        System.out.println(logNote);
        log += logNote;

        // Check to see if the first letter of MetricType is capitalised.
        String convertedMetric = controllerUtils.capitaliseFirstLetter(metricType);

        // The buttons are wrapped in a div with the id of the supertype name 
        // + buttons, e.g. "<div id="ServerButtons>".
        String valueTags = "<div id=\"" + superClass + convertedMetric + "\">";
        ArrayList<ModelObjectInterface> modelObjectList = modelObjectPool.getModelObjectsBySuperClass(superClass);
        for (ModelObjectInterface modelObject : modelObjectList) {
            valueTags += "<div id=\"" + modelObject.getName() + convertedMetric + "\">" + modelObject.getMetric(convertedMetric) + "</div>";
        }

        valueTags += "</div>";
        return valueTags;
    }

    /**
     * @return The {@link mvc.model.AbstractModelObject#name name} of every
     * object contained within
     * {@link mvc.controller.ServletProxy#modelObjectPool} formatted into HTML
     * tags.
     */
    public String getObjectNameList() {

        String formattedNameList = "<div id=\"ObjectNames\">";

        for (ModelObjectInterface modelObject : modelObjectPool.getModelObjectList()) {
            formattedNameList += "<div id =\"" + modelObject.getName() + "_Name\">" + modelObject.getName() + ",</div>";
        }

        formattedNameList += "</div>";

        return formattedNameList;
    }

    /**
     * @param metricType An attribute to retrieve from every object in
     * {@link mvc.controller.ServletProxy#modelObjectPool}.
     *
     * @return The specified attribute for every object in
     * {@link mvc.controller.ServletProxy#modelObjectPool}, formatted into HTML
     * tags.
     */
    public String getObjectMetrics(String metricType) {
        String output = "";

        for (String table : filteredTableList) {
            String capdTable = controllerUtils.capitaliseFirstLetter(table);

            // Check to see if there are any objects in the table: if there are 
            // none, nothing (not even the label) is printed for that table.
            if (modelObjectPool.getModelObjectsBySuperClass(capdTable).size() > 0) {
                output += getObjectMetricsBySuperclass(capdTable, metricType);
            }
        }
        return output;
    }

    /**
     * Display all object metrics for all objects as a single HTML formatted
     * String by calling
     * {@link mvc.controller.ServletProxy#getObjectMetricsBySuperclass(java.lang.String, java.lang.String)}
     * every table in {@link mvc.controller.ServletProxy#filteredTableList}.
     *
     * @see
     * mvc.controller.ServletProxy#getObjectMetricsBySuperclass(java.lang.String,
     * java.lang.String)
     * @see utils.ControllerUtils#capitaliseFirstLetter(java.lang.String)
     * @see
     * mvc.controller.modelobject.ModelObjectPool#getModelObjectsBySuperClass(java.lang.String)
     * @see mvc.model.AbstractModelObject#getMetricAttributeList()
     *
     * @return all metric data from all model objects, as a single HTML
     * formatted String.
     */
    public String getAllObjectMetrics() {
        String output = "";

        for (String table : filteredTableList) {
            String capdTable = controllerUtils.capitaliseFirstLetter(table);

            if (modelObjectPool.getModelObjectsBySuperClass(capdTable).size() > 0) {
                ArrayList<ModelObjectInterface> objects = modelObjectPool.getModelObjectsBySuperClass(capdTable);

                for (ModelObjectInterface object : objects) {
                    ArrayList<String> attributes = object.getMetricAttributeList();

                    for (String attribute : attributes) {
                        String capdAttr = controllerUtils.capitaliseFirstLetter(attribute);
                        output += getObjectMetricsBySuperclass(capdTable, capdAttr);
                    }
                }
            }
        }
        return output;// + "</div>";
    }

    /**
     * Return the names of all of the model objects in a single HTML formatted
     * string.
     *
     * @return A list of all of the {@link mvc.model.AbstractModelObject#name}
     * attributes of all model objects in a single HTML formatted string.
     */
    public String generateObjectMetricNameList() {
        String output = "<div id=\"metricNamesList\">";
        for (String table : filteredTableList) {
            String capdTable = controllerUtils.capitaliseFirstLetter(table);
            ArrayList<ModelObjectInterface> objects = modelObjectPool.getModelObjectsBySuperClass(capdTable);

            for (ModelObjectInterface object : objects) {
                output += "<div id=\"" + object.getName() + "MetricsList\">";//

                for (String attribute : object.getMetricAttributeList()) {
                    output += "<div>" + attribute + "</div>";
                }
                output += "</div>";
            }
        }

        return output + "</div>";
    }

    /**
     * Returns the relative URL for the three.js JSON file of every object in
     * {@link mvc.controller.ServletProxy#modelObjectPool} of a specified type,
     * such as 'resources/JSON/DemoServer.js'.
     *
     * @param superClass The
     * {@link mvc.model.AbstractModelObject#superclass superclass} to filter
     * objects in {@link mvc.controller.ServletProxy#modelObjectPool} by.
     *
     * @return The {@link mvc.model.AbstractModelObject#url_JSON} attribute of
     * every object in {@link mvc.controller.ServletProxy#modelObjectPool},
     * formatted into HTML tags.
     */
    public String getObjectJSONStrings(String superClass) {
        String logNote = "ServletProxy#getObjectJSONStrings(" + superClass + ") called.";
        System.out.println(logNote);
        log += logNote;

        // The buttons are wrapped in a div with the id of the supertype name 
        // + buttons, e.g. "<div id="ServerButtons>".
        String jsonDataTags = "<div id=\"" + superClass + "JSON\">";
        ArrayList<ModelObjectInterface> modelObjectList = modelObjectPool.getModelObjectsBySuperClass(superClass);
        for (ModelObjectInterface modelObject : modelObjectList) {
            jsonDataTags += "<div id=\"" + modelObject.getName() + "JSON\">" + modelObject.getJSON_File() + "</div>";
        }

        jsonDataTags += "</div>";
        return jsonDataTags;
    }

    /**
     * Returns the JSON data required to construct a 3D object using a
     * JSONLoader object in the three.js library. This method does this for
     * every object in {@link mvc.controller.ServletProxy#modelObjectPool} with
     * a {@link mvc.model.AbstractModelObject#superclass} which matches this
     * method's parameter.
     *
     * @param superClass The
     * {@link mvc.model.AbstractModelObject#superclass superclass} to filter
     * objects in {@link mvc.controller.ServletProxy#modelObjectPool} by.
     *
     * @return The {@link mvc.model.AbstractModelObject#json_data} attribute of
     * every object in {@link mvc.controller.ServletProxy#modelObjectPool},
     * formatted into HTML tags.
     */
    private String getObjectJSONModels(String superClass) {
        String logNote = "ServletProxy#getObjectJSONModels(" + superClass + ") called.";
        System.out.println(logNote);
        log += logNote;

        // The buttons are wrapped in a div with the id of the supertype name 
        // + buttons, e.g. "<div id="ServerButtons>".
        String modelDataTags = "<div id=\"" + superClass + "Models\">";
        ArrayList<ModelObjectInterface> modelObjectList = modelObjectPool.getModelObjectsBySuperClass(superClass);
        for (ModelObjectInterface modelObject : modelObjectList) {
            modelDataTags += "<div id=\"" + modelObject.getName() + "Model\">" + modelObject.getJson_data() + "</div>";
        }

        modelDataTags += "</div>";
        return modelDataTags;
    }

    /**
     * Create a HTML formatted drop-down list of all objects in
     * {@link mvc.controller.modelobject.ModelObjectPool}.
     *
     * @see utils.ControllerUtils#filterOut(java.util.ArrayList,
     * java.lang.String)
     *
     * @see utils.ControllerUtils#capitaliseFirstLetter(java.lang.String)
     *
     * @param superclass The superclass of the objects to be added to the
     * drop-down list.
     *
     * @return A HTML formatted drop-down list of all objects in
     * {@link mvc.controller.modelobject.ModelObjectPool} who's superclass is of
     * the supplied superclass parameter.
     */
    public String formatObjectsAsDropdownList(String superclass) {

        ArrayList<ModelObjectInterface> modelObjectList = modelObjectPool.getModelObjectsBySuperClass(superclass);

        String formattedString = "<select name=\"object\">";
        for (ModelObjectInterface modelObject : modelObjectList) {
            System.out.println(modelObject.getName());
            formattedString += "<option value=\"" + modelObject.getName() + "\">" + modelObject.getName() + "</option>";
            formattedString += modelObject.getName();
        }
        return formattedString;
    }

    /**
     * @see utils.ControllerUtils#filterOut(java.util.ArrayList,
     * java.lang.String)
     *
     * @see utils.ControllerUtils#capitaliseFirstLetter(java.lang.String)
     * @see
     * mvc.controller.ServletProxy#formatObjectsAsDropdownList(java.lang.String)
     *
     * @return drop-down lists for ever object type, making sure to exclude the
     * administrator table from the list of objects.
     */
    public String makeAllDropdownLists() {
        String output = "";

        for (String table : filteredTableList) {
            if (filteredTableList.size() > 0) {
                String capdTable = controllerUtils.capitaliseFirstLetter(table);

                // Check to see if there are any objects in the table: if there are 
                // none, nothing (not even the label) is printed for that table.
                if (modelObjectPool.getModelObjectsBySuperClass(capdTable).size() > 0) {
                    output += "<h2>" + capdTable + "</h2>";
                    output += formatObjectsAsDropdownList(capdTable);
                    output += "<input type=\"hidden\" name=\"type\" value=\"" + capdTable + "\">";
                    output += "<input class=\"controlButton\" type=\"submit\" value=\"Delete Object\">";
                }
            } else {
                output += "<div class=\"failMessage\"><span class=\"importantText\"><p>You have not objects to delete</p></span><p>Please add some objects to the database first.</p></div>";
            }
        }
        return output;
    }

    /**
     * @see
     * mvc.controller.ServletProxy#formatJsonUrisOfTypeAsButtons(java.lang.String)
     *
     * @see utils.ControllerUtils#capitaliseFirstLetter(java.lang.String)
     *
     * @return For each object type, a label and list of buttons bearing a label
     * and id matching their name attribute.
     */
    public String getObjectButtons() {
        String output = "<div id=\"objectSelectionButtons\">";

        for (String table : filteredTableList) {
            String capdTable = controllerUtils.capitaliseFirstLetter(table);

            // Check to see if there are any objects in the table: if there are 
            // none, nothing (not even the label) is printed for that table.
            if (modelObjectPool.getModelObjectsBySuperClass(capdTable).size() > 0) {
                output += "<div class=\"labelSmall objectButtonsDrawerLabel\">" + capdTable + "</div>";
                output += "<div class=\"objectButtonsDrawer\" id='" + capdTable + "'>";
                output += formatJsonUrisOfTypeAsButtons(capdTable);
                output += "</div><br>";
            }
        }
        return output + "</div></div>";
    }

    public String makeJSONModels() {

        String output = "<div id=\"jsonModels\">";

        for (String table : filteredTableList) {
            String capdTable = controllerUtils.capitaliseFirstLetter(table);

            // Check to see if there are any objects in the table: if there are 
            // none, nothing (not even the label) is printed for that table.
            if (modelObjectPool.getModelObjectsBySuperClass(capdTable).size() > 0) {
                output += getObjectJSONModels(capdTable);
            }
        }
        return output + "</div>";
    }

    /**
     * Allows for the regeneration of the model (i.e.
     * {@link mvc.controller.modelobject.ModelObjectPool} being purged and the
     * list of {@link mvc.model.ModelObjectInterface} implementing objects being
     * regenerated and re-added to
     * {@link mvc.controller.modelobject.ModelObjectPool}).
     *
     * This allows for the model to be dynamically regenerated by servlets at
     * runtime.
     *
     * @see mvc.controller.modelobject.ModelObjectFactory#execute()
     */
    public void regenerateModel() {
        modelObjectFactory.execute();
    }
}
