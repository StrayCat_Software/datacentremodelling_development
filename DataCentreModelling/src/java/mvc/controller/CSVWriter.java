package mvc.controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import utils.SiteUtils;

/**
 * Writes to the CSV files.
 * 
 * @author Daniel Pawsey
 * 
 * @deprecated Use {@link mvc.controller.DatabaseTools.DatabaseWriter} instead.
 */
public class CSVWriter {

    private String file;
    private String path;
    private SiteUtils siteUtils;
    
    public CSVWriter(){
        siteUtils = new SiteUtils();
    }

    /**
     * Set the {@link mvc.controller.CSVWriter#path} of the file to read.
     * 
     * @param path The value to assign to {@link mvc.controller.CSVWriter#path}, as a String
     */
    public void setPath(String path) {
        this.path = path;
        System.out.println("path = " + this.path);
    }
    
    /**
     * Set the file to read.
     * 
     * @param fileName The name of the file to read, as a String
     * @param request the calling HttpRequest.
     */
    public void setFile(String fileName, HttpServletRequest request) {
        file = siteUtils.getURL(request) + path + fileName + ".csv";
        System.out.println("file = " + file);
    }
    
    /**
     * Get {@link mvc.controller.CSVWriter#file}.
     * 
     * @return {@link mvc.controller.CSVWriter#file}
     */
    public String getFullPath(){
        return file;
    }
    
    /**
     * Read all comma-separated attributes from the file content.
     * 
     * @param attributesList The attributes write to of the file as a row.
     */
    public void writeRecord(ArrayList<String> attributesList) {
        String record = "";

        for (String attribute : attributesList) {
            record += attribute + ",";
        }
        writeToFile(record);
    }

    /**
     * Write a row to the CSV file.
     * 
     * @param record the CSV row to write to the file.
     */
    public void writeToFile(String record) {
        try {

            BufferedWriter bufferedWriter = new BufferedWriter(
                    new FileWriter("https://www.dropbox.com/s/bhxaxsuay091fmc/Server.csv?dl=0"));
            
                bufferedWriter.newLine();
                bufferedWriter.write(record);
                
        } catch (IOException ex) {
            Logger.getLogger(CSVWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
