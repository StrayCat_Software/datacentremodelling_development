/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.testharness;

import utils.CredentialChecker;

/**
 *
 * @author Dan
 */
public class CredentialCheckerHarness {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CredentialChecker cc = new CredentialChecker();
        boolean cred = cc.checkCredentials("wxu13keu", "abc123");
        
        if(cred){
            System.out.println("login successful");
        }
        else{
            System.out.println("login failed");
        }
    }
    
}
