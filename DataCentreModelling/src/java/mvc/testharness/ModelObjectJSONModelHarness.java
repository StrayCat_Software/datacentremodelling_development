package mvc.testharness;

import mvc.controller.modelobject.ModelObjectFactory;

/**
 *
 * @author Daniel Pawsey
 * @since 31/07/2015
 */
public class ModelObjectJSONModelHarness {
    
    public static void main(String[] args) {
        ModelObjectFactory fac = new ModelObjectFactory();
        fac.execute();
    }
    
}
