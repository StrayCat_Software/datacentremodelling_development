package mvc.testharness;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import mvc.controller.CSVWriter;

/**
 *
 * @author Daniel Pawsey
 */
public class CSVWriterHarness {

    public static void main(String[] args) {
        CSVWriter csvWriter = new CSVWriter();
        String type = "Server";

        ArrayList<String> attributesList = new ArrayList<String>();

        String name = "TestAddition";

        attributesList.add(name);

        String kWh = "11";

        attributesList.add(kWh);

        String co2 = "22";

        attributesList.add(co2);

        String temp = "33";

        attributesList.add(temp);

        String tempThreshold = "44";

        attributesList.add(tempThreshold);

        csvWriter.setPath("resources/data/");
//        csvWriter.setFile(type, request);
        csvWriter.writeRecord(attributesList);
    }
}
