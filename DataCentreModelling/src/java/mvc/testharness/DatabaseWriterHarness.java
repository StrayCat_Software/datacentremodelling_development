package mvc.testharness;

import java.util.ArrayList;
import mvc.controller.DatabaseTools.DatabaseWriter;

/**
 *
 * @author Daniel Pawsey
 * @since 23/07/2015
 */
public class DatabaseWriterHarness {
    
    public static void main(String[] args){
        DatabaseWriter dbw = new DatabaseWriter();
        ArrayList<String> attributes = new ArrayList<String>();
        attributes.add("TestInsertServer");
        attributes.add("1.0");
        attributes.add("1.0");
        attributes.add("1.0");
        attributes.add("1.0");
        dbw.writeRecord("Server", attributes);
        dbw.deleteRecord("Server", "TestInsertServer");
    }
}
