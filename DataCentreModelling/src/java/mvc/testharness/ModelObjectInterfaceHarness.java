package mvc.testharness;

import java.util.ArrayList;
import mvc.controller.modelobject.ModelObjectPool;
import mvc.model.ModelObjectInterface;
import mvc.model.crac.ConcreteCrac;
import mvc.model.server.ConcreteServer;

/**
 * A test harness class, intended to be used for any class which implements
 * {@link mvc.model.ModelObjectInterface ModelObjectInterface}.
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 25/05/2015
 */
public class ModelObjectInterfaceHarness {

    /**
     * Test harness static main method.
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Set up.
        ModelObjectInterface demo = new ConcreteServer();
        ModelObjectInterface demo2 = new ConcreteCrac();

        // Add all ModelObjectInterface objects to the pool.
        ModelObjectPool poolInstance = ModelObjectPool.getInstance();
        poolInstance.addModelObject(demo);
        poolInstance.addModelObject(demo2);
        
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//
        //             Test of ModelObjectInterface.getUrl_JSON               //
        //                  implementation by ConcreteServer                      //
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//
        System.out.println("\n\n**** Test of ModelObjectInterface.getUrl_JSON "
                + "implementation by ConcreteServer ****");

        // Iterate through all objects in the pool, and print their JSON 
        // file-paths.
        for (ModelObjectInterface modelObject : poolInstance.getModelObjectList()) {
            // print the JSON 3D object file URL of the created object.
            System.out.println(
                    "ModelObjectInterfaceHarness says that "
                    + modelObject.getClass() + "'s JSON 3D object file-path is "
                    + modelObject.getJSON_File()
            );
        }
        
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//
        //      Test of ModelObjectInterface.getModelObjectsBySuperClass       //
        //                implementation by ConcreteServer                        //
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//
        System.out.println(
                "\n\n**** Test of ModelObjectInterface.getSuperclassName "
                + "implementation by ConcreteServer ****"
        );
        
        // Iterate through all objects in the pool, and print their 
        // superclass names.
        for (ModelObjectInterface modelObject : poolInstance.getModelObjectList()) {
            // print the superclass name of the created object.
            System.out.println(
                    "ModelObjectInterfaceHarness says that "
                    + modelObject.getClass() + "'s superclass is "
                    + modelObject.getSuperclassName()
            );
        }
        
    }

}
