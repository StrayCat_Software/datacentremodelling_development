package mvc.testharness;

import mvc.controller.ServletProxy;

/**
 *
 * @author Daniel Pawsey
 */
public class DeleteObjectPageHarness {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ServletProxy sp = new ServletProxy();
        String serverList = sp.formatObjectsAsDropdownList("Server");
        String cracList = sp.formatObjectsAsDropdownList("CRAC");

    //
        System.out.println(serverList);
        System.out.println(cracList);
    }

}
