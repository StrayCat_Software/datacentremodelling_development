package mvc.testharness;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mvc.controller.DatabaseTools.DatabaseConnection;

/**
 * This class test-reads the model attribute of the DemoServer record of the 
 * server table.
 * 
 * @author Daniel Pawsey
 * @since 31/07/2015
 */
public class JSONFromDatabaseHarness {
    
    public static void main(String[] args) {
        try {
            DatabaseConnection dbc = new DatabaseConnection();
            String query = "SELECT model FROM server WHERE name = 'DemoServer'";
            ResultSet rs = dbc.query(query);
            int counter = 0;
            while(rs.next()){
                counter++;
                System.out.println("*** Result " + counter + " ****");
                System.out.println(rs.getString("model"));
                System.out.println("********************");
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(JSONFromDatabaseHarness.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       // JsonParser jp = new JsonParser();
    }
    
}
