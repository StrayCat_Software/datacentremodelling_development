/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.testharness;

import mvc.model.server.ConcreteServer;

/**
 *
 * @author Dan
 */
public class ConcreteServerHarness {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ConcreteServer demo = new ConcreteServer();
        demo.setCo2(3.3);
        
        System.out.println(demo.getMetric("Co2"));
    }
    
}
