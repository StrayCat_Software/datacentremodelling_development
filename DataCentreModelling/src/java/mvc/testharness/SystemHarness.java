/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.testharness;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mvc.controller.ServletProxy;

/**
 *
 * @author Dan
 */
public class SystemHarness {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ArrayList<String> fileNameList = new ArrayList<String>();
            String csvUrlString = "http://localhost:8080/DataCentreModelling/resources/data/DATA-MANIFEST.csv";
            URL csvFileUrl;
            csvFileUrl = new URL(csvUrlString);
            System.out.println("sucessfully parsed " + csvUrlString + " as a URL object");

            // Add each row from the specified file to fileNameList.
            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(csvFileUrl.openStream()))) {
                // Add each row from the specified file to fileNameList.
                String record;

                while ((record = reader.readLine()) != null) {
                    System.out.println(record);
                    fileNameList.add(record);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ServletProxy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
