package mvc.testharness;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mvc.controller.DatabaseTools.DatabaseConnection;

/**
 *
 * @author Daniel Pawsey
 * @since 31/07/2015
 */
public class DatabaseConnection_ReaderHarness {

    public static void main(String[] args) {
        try {
            DatabaseConnection dbc = new DatabaseConnection();
            String query = "SELECT * FROM server;";
            ResultSet rs = dbc.query(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int attributeCount = rsmd.getColumnCount();
            System.out.println("attribute count is " + attributeCount);

            int counter = 0;
            while (rs.next()) {
                counter++;
                System.out.println("*** Result " + counter + " ****");
                for (int i = 1; i <= attributeCount; i++) {
                    System.out.println(rs.getString(i));
                    
                }
                System.out.println("********************");
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(JSONFromDatabaseHarness.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
