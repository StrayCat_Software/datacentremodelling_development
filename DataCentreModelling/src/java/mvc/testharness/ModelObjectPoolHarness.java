package mvc.testharness;

import java.util.ArrayList;
import mvc.controller.modelobject.ModelObjectPool;
import mvc.model.ModelObjectInterface;
import mvc.model.crac.ConcreteCrac;
import mvc.model.server.ConcreteServer;

/**
 * Test harness class.
 *
 * @author Daniel Pawsey
 */
public class ModelObjectPoolHarness {

    static ArrayList<ModelObjectInterface> arrayList = new ArrayList<ModelObjectInterface>();

    /**
     * Static runtime.
     *
     * @param args Command-line arguments.
     *
     * @version 1.0
     * @since 22/05/2015
     */
    public static void main(String[] args) {
        try {
        // Set up.
            //Get an instance of the pool.
            ModelObjectPool poolInstance = ModelObjectPool.getInstance();

        // Construct an object which implements ModelObjectInterface, and add it
            // to the pool.
            ModelObjectInterface modelObject = new ConcreteServer();
            poolInstance.addModelObject(modelObject);

            // Construct and add a ConcreteCrac object, and add it to the pool.
            ModelObjectInterface modelObject2 = new ConcreteCrac();
            poolInstance.addModelObject(modelObject2);

            // Copy the ConcreteServer out of the pool, and print it's name.
            System.out.println("\n\n**** Test 1 ****");
            System.out.println(poolInstance.getModelObjectsBySuperClass("Server").get(0).getClass().getName());

            // Copy the ConcreteCrac out of the pool, and print it's name.
            System.out.println("\n\n**** Test 2 ****");
            System.out.println(poolInstance.getModelObjectsBySuperClass("CRAC").get(0).getClass().getName());

        // Attempt to get an object from the pool using the name of a 
            // super-class which does not exist.
//            System.out.println("\n\n**** Test 3 ****");
//            System.out.println(poolInstance.getModelObjectsBySuperClass("FailClass").get(0).getClass().getName());
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Index out of bounds exception:\n");
            ex.printStackTrace();
        }
    }
}
