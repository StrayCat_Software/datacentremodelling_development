package mvc.testharness;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mvc.controller.DatabaseTools.DatabaseConnection;

/**
 *
 * @author Dan
 */
public class DatabaseConnectionHarness {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        ArrayList<String> resultList = new ArrayList<String>();
        try {
            DatabaseConnection dbc = new DatabaseConnection();
            ResultSet resultSet = dbc.query("SELECT * FROM Server;");
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

            int columnsNumber = resultSetMetaData.getColumnCount();

            while (resultSet.next()) {
                String result = "";
                for (int i = 1; i <= columnsNumber; i++) {
                    result += resultSet.getString(i) + ",";
                }

                resultList.add(result);
                
                for(String printResult : resultList){
                    System.out.println(printResult);
                }
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DatabaseConnectionHarness.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
