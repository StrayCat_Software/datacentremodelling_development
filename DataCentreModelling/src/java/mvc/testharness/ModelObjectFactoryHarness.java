package mvc.testharness;

/**
 * @version 1.0
 * @since 21/05/2015
 */
import mvc.controller.modelobject.ModelObjectFactory;

/**
 * Test harness class.
 * 
 * @author Daniel Pawsey
 */
public class ModelObjectFactoryHarness {

    /**
     * Static runtime.
     * 
     * @param args Command-line arguments.
     */
    /*
    public static void main(String[] args) {
        ModelObjectFactory modelObjectFactory = new ModelObjectFactory();
        modelObjectFactory.retrieveFilesFromManifest("http://localhost:8080/DataCentreModelling/");
    }
    */
    
    public static void main(String[] args) {
        ModelObjectFactory modelObjectFactory = new ModelObjectFactory();
//        modelObjectFactory.readTable("Server");
    }
}
