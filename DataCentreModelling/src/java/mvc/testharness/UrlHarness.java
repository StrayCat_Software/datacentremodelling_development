/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.testharness;

import java.net.URL;

/**
 *
 * @author Dan
 */
public class UrlHarness {
    
    private UrlHarness(){
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UrlHarness urlHarness = new UrlHarness();
        String fileName = "web/index.html";
        System.out.println(fileName);
        System.out.println(new UrlHarness().getClass().getResource(fileName).toString());
        System.out.println(new UrlHarness().getClass().getClassLoader().getResource(fileName));
    }
}
