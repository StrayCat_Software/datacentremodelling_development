package mvc.testharness;

import mvc.controller.ServletProxy;

/**
 *
 * @author Daniel Pawsey
 */
public class VCSynapseHarness {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ServletProxy sp = new ServletProxy();
        String buttons = sp.getObjectButtons();
        String getObjectNameList = sp.getObjectNameList();
        String metricsNameList = sp.generateObjectMetricNameList();
        String allObjectMetrics = sp.getAllObjectMetrics();
        String jsonModels = sp.makeJSONModels();

        System.out.println(buttons);
        System.out.println(getObjectNameList);
        System.out.println(metricsNameList);
        System.out.println(allObjectMetrics);
        System.out.println(jsonModels);
    }

}
