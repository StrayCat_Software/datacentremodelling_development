/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.testharness;

import java.util.ArrayList;
import mvc.controller.DatabaseTools.DatabaseConnection;

/**
 *
 * @author Dan
 */
public class DatabaseConnection_GetMetadataHarness {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DatabaseConnection dbc = new DatabaseConnection();
        ArrayList<String> tablesList = dbc.getTables();
        
        for(String table : tablesList){
            System.out.println(table);
        }
    }
    
}
