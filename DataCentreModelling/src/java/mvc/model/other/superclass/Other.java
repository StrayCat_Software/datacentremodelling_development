/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.model.other.superclass;

import mvc.model.AbstractModelObject;

/**
 * This class holds all attributes common to all Other 3D objects in the game.
 * Other objects are unclassified objects.
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 05/08/2015
 */
public abstract class Other extends AbstractModelObject {

}
