/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.model.other;

import mvc.model.ModelObjectInterface;
import mvc.model.crac.superclass.Crac;
import mvc.model.other.superclass.Other;

/**
 * This class holds attributes for a single Other object.
 *
 * @author Daniel Pawsey
 * @since 05/08/2015
 */
public class ConcreteOther extends Other implements ModelObjectInterface{

    /**
     * Concatenates the URL of the JSON file onto
     * {@link model.servers.AbstractServer#serverPath serverPath} string of
     * {@link model.servers.AbstractServer AbstractServer}. This URL can be
     * called by items such as on-page buttons, allowing the URL of the 3D JSON
     * file to be drawn to be assigned by retriving it as a String from this
     * class.
     *
     *
     */
    private int url_JSON;

    /**
     * <p>
     * postfixes the file-name of the data file storing information used to
     * populate attributes of this class to the file-path as far as
     * web/resources/data/model/server so far constructed as a String by
     * {@link src.model.server.AbstractServer AbstractServer}.</p>
     *
     * <p>
     * This string is called as a parameter by this classes method
     * {@link model.server.ServerType1#readServerType1AttributesFromFile(String url_data) readServerType1Attribute}.</p>
     *
     */
    private int url_data;

    /**
     * <p>
     * This method is called by this classes inheriting classes' methods
     * responsible for populating their attributes from a data file. It
     * populates data fields of this class by reading the data file into them,
     * and calls
     * {@link src.model.AbstractServer#readAbstractServerAttributesFromFile(String data_url) readAbstractServerAttributesFromFile}(String{@link src.model.server.ServerType1#url_data url_data}),
     * which calls
     * model.AbstractModelObject#readAbstractServerAttributesFromFile(String
     * url_data) readAbstractServerAttributesFromFile(String url_data)} to
     * populate inherited attributes. As such, populating the attributes of this
     * class is the result of a chain of file-reading methods.</p>
     * <p>
     * This method is called by the constructor of this class.</p>
     *
     * @param {@link model.server.url_data url_data} The file-path of the data
     * file to be read to populate the attributes of this class.
     *
     *
     */
    private void readAttributesFromFile(int url_data) {

    }

}
