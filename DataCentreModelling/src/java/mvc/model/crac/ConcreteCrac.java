package mvc.model.crac;

import mvc.model.crac.superclass.Crac;
import mvc.model.ModelObjectInterface;

/**
 * This class holds attributes for a single server object.
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 15/05/2015
 *
 */
public class ConcreteCrac extends Crac implements ModelObjectInterface {

    /**
     * Concatenates the URL of the JSON file onto
     * {@link model.servers.AbstractServer#serverPath serverPath} string of
     * {@link model.servers.AbstractServer AbstractServer}. This URL can be
     * called by items such as on-page buttons, allowing the URL of the 3D JSON
     * file to be drawn to be assigned by retriving it as a String from this
     * class.
     *
     *
     */
    private int url_JSON;

    /**
     * <p>
     * postfixes the file-name of the data file storing information used to
     * populate attributes of this class to the file-path as far as
     * web/resources/data/model/server so far constructed as a String by
     * {@link src.model.server.AbstractServer AbstractServer}.</p>
     *
     * <p>
     * This string is called as a parameter by this classes method
     * {@link model.server.ServerType1#readServerType1AttributesFromFile(String url_data) readServerType1Attribute}.</p>
     *
     */
    private int url_data;

}
