package mvc.model.crac.superclass;

import mvc.model.AbstractModelObject;

/**
 * This class holds all attributes common to all Crac 3D objects in the game. 
 * 
 * @author Daniel Pawsey
 * @version 1.0
 * @since 15/05/2015
 */
public abstract class Crac extends AbstractModelObject {

}
