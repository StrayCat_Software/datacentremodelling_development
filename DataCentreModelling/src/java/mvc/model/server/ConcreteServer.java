package mvc.model.server;

import mvc.model.server.superclass.Server;
import mvc.model.ModelObjectInterface;

/**
 * This class holds attributes for a single server object.
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 22/05/2015
 *
 */
public class ConcreteServer extends Server implements ModelObjectInterface {

    /**
     * <p>
     * postfixes the file-name of the data file storing information used to
     * populate attributes of this class to the file-path as far as
     * web/resources/data/model/server so far constructed as a String by
     * {@link src.model.server.AbstractServer Server}.</p>
     *
     * <p>
     * This string is called as a parameter by this class's method
     * {@link model.server.ServerType1#readServerType1AttributesFromFile(String url_data) readServerType1Attribute}.</p>
     *
     */
    private String url_data;
    
    /**
     * <p>
     * This method is called by this classes inheriting classes methods
     * responsible for populating their attributes from a data file. It
     * populates data fields of this class by reading the data file into them,
     * and calls
     * {@link model.AbstractServer#readAbstractServerAttributesFromFile(int) readAbstractServerAttributesFromFile},
     * which calls
     * {@link model.AbstractModelObject#readAbstractModelAttributesFromFile(int) readAbstractServerAttributesFromFile}
     * to populate inherited attributes. As such, populating the attributes of
     * this class is the result of a chain of file-reading methods.</p>
     * 
     * <p>
     * This method is called by the constructor of this class.</p>
     *
     * @param {@link model.server.url_data url_data} The file-path of the data
     * file to be read to populate the attributes of this class.
     *
     *
     */
    private void readAttributesFromFile(int url_data) {

    }
    
}