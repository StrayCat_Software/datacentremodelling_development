package mvc.model.server.superclass;

import mvc.model.AbstractModelObject;

/**
 * This class holds all attributes common to all server 3D objects in the game.
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 15/05/2015
 */
public abstract class Server extends AbstractModelObject {

    /**
     * This method is called by this classes inheriting classes methods
     * responsible for populating their attributes from a data file.
     *
     * @param url_data The file-path of the data file to be read to populate the
     * attributes of this class. This is passed to this method by an inheriting
     * subclass's method.
     */
    protected void readAbstractServerAttributesFromFile(int url_data) {

    }
    
}
