package mvc.model;

import java.util.ArrayList;

/**
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 15/05/2015
 */
public interface ModelObjectInterface {

    /**
     * <p>
     * It is necessary for
     * {@link mvc.controller.modelobject.ModelObjectPool#modelObjectList} to be
     * filtered by element superclass. This method enforces that implementing
     * classes can do this.</p>
     *
     * <p>
     * For all concrete mvc.model objects, this method is called by
     * {@link mvc.controller.modelobject.ModelObjectPool#getModelObjectsBySuperClass(java.lang.String)}.</p>
     *
     * @return The name of an implementing classes superclass.
     */
    public abstract String getSuperclassName();
    
    /**
     * <p>
     * Calls superclass members such as
     * {@link mvc.model.AbstractModelObject#abstractModelPath_data abstractModelPath_data}
     * and concatenates it to members of super-classes which extend
     * {@link mvc.model.AbstractModelObject}, such as 
     * {@link mvc.model.server.superclass.Server#abstractModelObject_Path_JSON abstractModelObject_Path_JSON}
     * and finally to the classes {@link java.lang.Object#getClass() getClass}
     * and a JavaScript ".js" file extention, resulting in the file-path of the
     * 3D object JSON file returned as a String.</p>
     *
     * <p>
     * This method is included in this interface to enforce that implementing
     * classes have the getJSON_File method. This is included because all
     * implementing classes should have it, and so that this method can be
     * called if the class is declared to be
     * {@link mvc.model.ModelObjectInterface ModelObjectInterface} despite
     * their constructor declarations can still use this method, implementing
     * polymorphism.</p>
     *
     * @return The file-path of the 3D object JSON file represented by the
     * implementing class.
     */
    public abstract String getJSON_File();
    
    /*
     * Attribute access methods.
     */
    
    public abstract void setKWh(double kWh);
    
    public abstract double getKWh();
    
    public abstract void setCo2(double co2);
    
    public abstract double getCo2();
    
    public abstract void setTemp(double co2);
    
    public abstract double getTemp();
    
    public abstract double getMetric(String metricType);
    
    public abstract void setTempThreshold(double tempThreshold);
    
    public abstract double getTempThreshold();
    
    public abstract void setName(String name);
    
    public abstract String getName();
    
    public abstract void setJson_data(String json_data);
    
    public abstract String getJson_data();
    
    public abstract void setSuperclass(String superclass);
    
    public abstract String getSuperclass();
    
    public abstract ArrayList<String> getMetricAttributeList();
}
