package mvc.model;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class serves contains all attributes common to all 3D objects in the
 * game.
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 15/05/2015
 *
 */
public abstract class AbstractModelObject {
    /* {author=Daniel Pawsey
     , version=1.0
     , since=15/05/2015
 
     }*/

    private final String abstractModelObject_Path_JSON = "resources/JSON/";

    /**
     * The name of the item represented.
     */
    protected String name;
    
    /**
     * This will be used in concrete classes which inherit from this class, and
     * will concatenate
     * {@link mvc.model.AbstractModelObject#abstractModelObject_Path_JSON abstractModelObject_Path_JSON}
     * to any folder directories provided by inheriting abstract classes, and
     * the file-name provided by inheriting concrete classes to result in a full
     * relative file-path for a 3D JSON object file which is represented by this
     * class.
     */
    private String url_JSON;

    /**
     * This defines the file-path for this object as far as the
     * web/resources/data folder. It is prefixed onto the file-path Strings of
     * inheriting classes, extending the file-path.
     *
     *
     */
    private int abstractModelPath_data;

    /**
     * The in-game energy use of the object represented by this object, measured
     * in kilowatt-hours.
     */
    private double kWh;

    /**
     * The carbon-dixoide output per year of the object represented by this
     * object, measured in tonnes of carbon-dioxide per year.
     */
    private double co2;
    
    /**
     * The temperature change caused by adding this device to the environment.
     */
    private double temp;
    
    /**
     * the temperature threshold beyond which this item would not be able to 
     * function.
     */
    private double tempThreshold;
    
    /**
     * 
     */
    private String json_data;
    
    private String superclass;

    /**
     * <p>
     * Access method for any
     * {@link mvc.model.AbstractModelObject#abstractModelObject_Path_JSON abstractModelObject_Path_JSON}
     * implementations of concrete classes which inherit from this class.</p>
     *
     * <p>
     * This method is to be used when retrieving
     * {@link mvc.model.AbstractModelObject#abstractModelObject_Path_JSON abstractModelObject_Path_JSON}
     * for use in scenarios such as passing the URI to a JavaScript
     * function.</p>
     *
     * @see mvc.model.ModelObjectInterface#getJSON_File()
     *
     * @return The relative file-path of the 3D JSON object which represents
     * this class.
     */
    public String getJSON_File() {
        return abstractModelObject_Path_JSON + this.getClass().getSuperclass().getSimpleName() + ".js";
    }

    /**
     * Ensures that inheriting classes implement
     * {@link mvc.model.ModelObjectInterface#getSuperclassName().}
     *
     * @return The name of the immediate superclass of any concrete classes
     * inheriting from this class.
     */
    public String getSuperclassName() {
        /* {return=The name of the immediate superclass of any concrete classes
         inheriting from this class.
         }*/

        return this.getClass().getSuperclass().getSimpleName();
    }
    
    /**
     * Access method which allows a specific attribute to be requested.
     * 
     * @param metricType The name of the attribute to be returned, as a String.
     * 
     * @return The requested metric, such as kWh or co2.
     */
    public double getMetric(String metricType){
        double output = 0;
        
        if(metricType.equals("KWh")){
            output = this.kWh;
        }
        if(metricType.equals("Co2")){
            output = this.co2;
        }
        if(metricType.equals("Temp")){
            output = this.temp;
        }
        if(metricType.equals("TempThreshold")){
            output = this.tempThreshold;
        }
        
        return output;
    }
    
    /*
    Reflective methods
    */
    
    /**
     * @return An ArrayList of all of the classes from which this class 
     * inherits, excluding {@link java.lang.Object}
     */
    private ArrayList<Class> getSuperclassList(){
        ArrayList<Class> superclassList = new ArrayList();
        Class<?> current = this.getClass();
        while (current.getSuperclass() != null) {
            current = current.getSuperclass();
            if (!current.getName().equals("java.lang.Object")) {
                superclassList.add(current);
            }
        }
        return superclassList;
    }
    
    /**
     * @return An ArrayList of simplified strings (i.e. excluding the package 
     * and class names provided by {@link java.lang.reflect.Field)) of the 
     * names of all of the attributes which this class inherits from any 
     * superclasses.
     * 
     * @see mvc.model.AbstractModelObject#getSuperclassList()
     */
    private ArrayList<String> getInheritedAttributes(){
        ArrayList<Class> superclassList = getSuperclassList();
        ArrayList<String> fieldNameStrings = new ArrayList();

        for (Class superclass : superclassList) {
            Field[] attributes = superclass.getDeclaredFields();
            for (Field attribute : attributes) {
                char[] attributeChars = attribute.toString().toCharArray();
                String backwardsSimpleAtrributeName = "";
                for (int i = attributeChars.length - 1; i >= 0; i--) {
                    if (attributeChars[i] != '.') {
                        backwardsSimpleAtrributeName += attributeChars[i];
                    } else {
                        break;
                    }
                }
                String simpleAtrributeName = new StringBuilder(backwardsSimpleAtrributeName).reverse().toString();
                fieldNameStrings.add(simpleAtrributeName);
            }
        }
        return fieldNameStrings;
    }
    
    /**
     * @return An ArrayList of simplified strings (i.e. excluding the package 
     * and class names provided by {@link java.lang.reflect.Field)) of the names 
     * of all of the attributes which are declared within this superclass, and 
     * which this class inherits from any superclasses.
     * 
     * @see mvc.model.AbstractModelObject#getInheritedAttributes()
     * @see mvc.model.AbstractModelObject#getSuperclassList()
     */
    private ArrayList<String> getAllAttributes(){
        ArrayList<String> superclassAttributes = getInheritedAttributes();
        Field[] classAttributes = this.getClass().getDeclaredFields();
        for (Field attribute : classAttributes) {
            char[] attributeChars = attribute.toString().toCharArray();
            String backwardsSimpleAtrributeName = "";
            for (int i = attributeChars.length - 1; i >= 0; i--) {
                if (attributeChars[i] != '.') {
                    backwardsSimpleAtrributeName += attributeChars[i];
                } else {
                    break;
                }
            }
            String simpleAtrributeName = new StringBuilder(backwardsSimpleAtrributeName).reverse().toString();
            superclassAttributes.add(simpleAtrributeName);
        }
        
        return superclassAttributes;
    }
    
    /**
     * @return An ArrayList of simplified strings (i.e. excluding the package 
     * and class names provided by {@link java.lang.reflect.Field} of the names 
     * of all of the attributes which are declared within this superclass, and 
     * which this class inherits from any superclasses, filtered by a group of 
     * attributes which are not to be used as metrics.
     * 
     * @see mvc.model.AbstractModelObject#getAllAttributes()
     * @see mvc.model.AbstractModelObject#getInheritedAttributes
     * @see mvc.model.AbstractModelObject#getSuperclassList()
     */
    public ArrayList<String> getMetricAttributeList(){
        ArrayList<String> attributes = getAllAttributes();
        
        attributes.remove("abstractModelObject_Path_JSON");
        attributes.remove("name");
        attributes.remove("url_JSON");
        attributes.remove("abstractModelPath_data");
        attributes.remove("json_data");
        attributes.remove("superclass");
        attributes.remove("url_data");
        
        return attributes;
    }

    /*
    Accessor methods.
    */
    
    public void setKWh(double kWh) {
        this.kWh = kWh;
    }

    public double getKWh() {
        return kWh;
    }

    public void setCo2(double co2) {
        this.co2 = co2;
    }

    public double getCo2() {
        return co2;
    }
    
    public void setTemp(double temp){
        this.temp = temp;
    }
    
    public double getTemp(){
        return temp;
    }
    
    public void setTempThreshold(double tempThreshold){
        this.tempThreshold = tempThreshold;
    }
    
    public double getTempThreshold(){
        return tempThreshold;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
    public void setJson_data(String json_data){
        this.json_data = json_data;
    }
    
    public String getJson_data(){
        return json_data;
    }
    
    public void setSuperclass(String superclass){
        this.superclass = superclass;
    }
    
    public String getSuperclass(){
        return superclass;
    }

}
