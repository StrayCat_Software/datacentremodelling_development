package mvc.controller;

import mvc.controller.modelobject.ModelObjectPool;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 23/05/2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    mvc.controller.ModelObjectFactoryTest.class, 
    mvc.controller.ModelObjectPoolTest.class}
)

public class ControllerTestSuite {

    /**
     * Ensure that the singleton pool is clear before beginning.
     * @throws Exception 
     */
    @Before
    public void setUp() throws Exception {
        ModelObjectPool pool = ModelObjectPool.getInstance();
        pool.clearModelObjectList();
    }

    /**
     * Ensure that the singleton pool is clear after finishing.
     * @throws Exception 
     */
    @After
    public void tearDown() throws Exception {
        ModelObjectPool pool = ModelObjectPool.getInstance();
        pool.clearModelObjectList();
    }
    
}
