/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.controller;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;

/**
 *
 * @author Daniel Pawsey
 * @version 1.0
 * @since 29/05/2015
 */
public class CSVReaderTest {

    /**
     * Test of retrieveRows method, of class CSVReader.
     */
    @Test
    public void testRetrieveRows() {
        System.out.println("retrieveRows");
        String filePath = "resources//data//DATA-MANIFEST.csv";

        CSVReader instance = new CSVReader();

        // The expected return is an ArrayList containing the Strings "Server" 
        // and "CRAC".
        ArrayList<String> expResult = new ArrayList<String>();
        String string1 = "Server", string2 = "CRAC";
        expResult.add(string1);
        expResult.add(string2);

        ArrayList<String> result = instance.retrieveRows(filePath);

        assertEquals(expResult, result);
    }

    /**
     * Test of readRecord method, of class CSVReader.
     */
    @Test
    public void testReadRecord() {
        System.out.println("readRecord");
        String record = "ConcreteServer,";
        CSVReader instance = new CSVReader();

        // The expected return is a String "ConcreteServer"- 
        // the parameter without the comma.
        ArrayList<String> expResult = new ArrayList<String>();
        expResult.add("ConcreteServer");
        ArrayList<String> result = instance.readRecord(record);
        assertEquals(expResult, result);
    }

}
