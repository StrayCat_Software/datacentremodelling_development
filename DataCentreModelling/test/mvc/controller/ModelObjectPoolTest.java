package mvc.controller;

import java.util.ArrayList;
import mvc.controller.modelobject.ModelObjectPool;
import mvc.model.ModelObjectInterface;
import mvc.model.crac.ConcreteCrac;
import mvc.model.server.ConcreteServer;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Daniel Pawsey
 * @since 21/05/2015
 * @version 1.0
 */
public class ModelObjectPoolTest {

    /**
     * Test of {@link mvc.controller.modelobject.ModelObjectPool#getInstance()}
     */
    @Test
    public void testGetInstance() {

        // Set up.
        System.out.println("getInstance");
        ModelObjectPool instance = ModelObjectPool.getInstance();

        // Assert that the instance is of class ModelObjectPool by comparing 
        // it's class name with the class name of a static instance of the class itself.
        assertEquals(
                instance.getClass().getName(),
                ModelObjectPool.class.getName());

        // Clear the pool for other tests.
        instance.clearModelObjectList();
    }

    /**
     * Test of
     * {@link mvc.controller.modelobject.ModelObjectPool#addModelObject(mvc.model.ModelObjectInterface)}
     */
    @Test
    public void testAddModelObject() {

        // Set up.
        System.out.println("addModelObject");
        ModelObjectPool instance = ModelObjectPool.getInstance();

        // Construct a ConcreteServer object, and add it to the pool.
        ModelObjectInterface modelObject = new ConcreteServer();
        modelObject.setName("Server");
        instance.addModelObject(modelObject);

        // Assert that the ConcreteServer returned from the pool is the same as the
        // ConcreteServer constructed locally.
        assertEquals(
                modelObject,
                instance.getModelObjectsByType("Server").get(0));

        // Clear the pool for other tests.
        instance.clearModelObjectList();
    }

    /**
     * Test of
     * {@link mvc.controller.modelobject.ModelObjectPool#getModelObjectbyIndex(int)}
     */
    @Test
    public void testGetModelObjectbyIndex() {

        // Set up.
        System.out.println("getModelObjectbyIndex");
        ModelObjectPool instance = ModelObjectPool.getInstance();
        
        // Construct a ConcreteServer object, and add it to the pool.
        ModelObjectInterface modelObject = new ConcreteServer();
        instance.addModelObject(modelObject);

        // Assert that the first object returned from the pool by querying it
        // by index is the same as the one constructed locally.
        int index = 0;
        assertEquals(modelObject, instance.getModelObjectbyIndex(index));

        // Clear the pool for other tests.
        instance.clearModelObjectList();
    }

    /**
     * Test of
     * {@link mvc.controller.modelobject.ModelObjectPool#getModelObjectList()}.
     */
    @Test
    public void testGetModelObjectList() {

        // Set up.
        System.out.println("getModelObjectList");
        ModelObjectPool instance = ModelObjectPool.getInstance();

        // Comparison ArrayList
        ArrayList<ModelObjectInterface> localList = new ArrayList<ModelObjectInterface>();

        // A ConcreteServer object is added to both the pool and the ArrayList
        ModelObjectInterface demo = new ConcreteServer();
        instance.addModelObject(demo);
        localList.add(demo);

        // Assert that the object returned from the pool and the local list is the same.
        assertEquals(localList.get(0), instance.getModelObjectList().get(0));

        // Clear the pool for other tests.
        instance.clearModelObjectList();
    }

    /**
     * Test of
     * {@link mvc.controller.modelobject.ModelObjectPool#getModelObjectsBySuperClass(java.lang.String)}.
     */
    @Test
    public void testGetModelObjectBySuperClass() {
        System.out.println("getModelObjectBySuperClass");

        // Set up.
        ModelObjectPool instance = ModelObjectPool.getInstance();
        // Comparison ArrayList.
        ArrayList<ModelObjectInterface> localList = new ArrayList<ModelObjectInterface>();

        // ModelObjectInterface implementing classes of different types are 
        // added to both the Pool and the local ArrayList.
        ModelObjectInterface demo = new ConcreteServer();
        instance.addModelObject(demo);
        localList.add(demo);

        ModelObjectInterface demo2 = new ConcreteCrac();
        instance.addModelObject(demo2);
        localList.add(demo2);

        // The first item from both lists is retrieved. In both cases, it 
        // should be an instance of the same ConcreteServer object.
        assertEquals(localList.get(0), instance.getModelObjectsBySuperClass("Server").get(0));

        // Clear the pool for other tests.
        instance.clearModelObjectList();
    }

}
