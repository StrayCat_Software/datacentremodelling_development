package mvc.controller;

import mvc.controller.modelobject.ModelObjectFactory;
import mvc.controller.modelobject.ModelObjectPool;
import mvc.model.server.ConcreteServer;
import static org.hamcrest.CoreMatchers.instanceOf;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dan
 * @version 1.0
 * @since 21/05/2015
 */
public class ModelObjectFactoryTest {

    /**
     * <p>
     * Test of
     * {@link mvc.controller.modelobject.ModelObjectFactory#retrieveFilesFromManifest()}</p>
     *
     * <p>
     * Works by having a
     * {@link controller.ModelObjectFactory ModelObjectFactory} object read the
     * data file, which results in a
     * {@link mvc.model.server.ConcreteServer ConcreteServer} object being added to
     * ModelObjectPool's
     * {@link controller.ModelObjectPool#modelObjectList modelObjectList}, which
     * is retrieved and is class checked.</p>
     *
     * <p>
     * Passing the test is contingent on the class-check returning that an
     * object of class {@link mvc.model.server.ConcreteServer ConcreteServer} is
     * returned from the {@link controller.ModelObjectPool ModelObjectPool}.</p>
     */
    @Test
    public void testRetrieveFilesFromManifest() {
        
        // Set up calls void method testRetrieveFilesFromManifest.
        System.out.println("readDataFiles");
        ModelObjectFactory instance = new ModelObjectFactory();
        instance.execute();

        // instance.retrieveFilesFromManifest() should have added a ConcreteServer 
        // object to ModelObjectPool.
        ModelObjectPool pool = ModelObjectPool.getInstance();
        assertThat(pool.getModelObjectsBySuperClass("Server").get(0),
                instanceOf(ConcreteServer.class)
        );
    }
    
    /**
     * Clear the pool for other test cases within the test suite.
     */
    @After
    public void tearDown() {
        ModelObjectPool pool = ModelObjectPool.getInstance();
        pool.clearModelObjectList();
    }

}
