/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.model;

import mvc.model.server.ConcreteServer;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dan
 */
public class ModelObjectInterfaceTest {

    public ModelObjectInterfaceTest() {}

    /**
     * Test of {@link mvc.model.ModelObjectInterface#getJSON_File()}.
     */
    @Test
    public void getJSON_File() {

        // Run test on ConcreteServer, a class which implements
        // ModelObjectInterface.
        System.out.println("getJSON_File");
        ModelObjectInterface instance = new ConcreteServer();

        // A prediction of what the file path retuned by the object will be.
        String testData = "{ testData: 'testData'}";
        instance.setJson_data(testData);
        String expResult = testData;

        // Assert that the file path returned by the object is the same as the 
        // predicted file path.
        assertEquals(expResult, instance.getJson_data());
    }

    /**
     * Test of {@link mvc.model.ModelObjectInterface#gtetSuperclassName()}.
     */
    @Test
    public void testGetSuperclassName() {

        // Run test on ConcreteServer, a class which implements
        // ModelObjectInterface
        System.out.println("getSuperclassName");
        ModelObjectInterface instance = new ConcreteServer();

        // A prediction of what the super-class name of the object will be.
        String expResult = "Server";

        // Assert that the super-class name returned by the object is the same 
        // as the predicted super-class name.
        assertEquals(expResult, instance.getSuperclassName());
    }

}
