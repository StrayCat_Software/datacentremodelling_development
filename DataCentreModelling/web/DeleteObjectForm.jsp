<%-- 
    Document   : DeleteObjectPage
    Created on : 23-Jul-2015, 21:25:42
    Author     : Daniel Pawsey
--%>

<%@page import="mvc.controller.ServletProxy"%>
<%
    ServletProxy sp = new ServletProxy();
    String serverList = sp.formatObjectsAsDropdownList("Server");
    String cracList = sp.formatObjectsAsDropdownList("Crac");
    String dropdowns = sp.makeAllDropdownLists();
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Object deletion form | Data Centre Modelling</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body>
        <div id="pageForm">
            <h1>Delete an object from the application</h1>
            <form action="DeleteObjectServlet" method="GET">
                <%=dropdowns%>
            </form>
        </div>
    </body>
</html>
