<%-- 
    Document   : View-Controller_Synapse
    Created on : 01-Jun-2015, 12:01:25 
    Author     : Daniel Pawsey
--%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.File"%>
<%@page import="java.io.InputStream"%>
<%@page import="mvc.controller.modelobject.ModelObjectFactory"%>
<%@page import="mvc.controller.modelobject.ModelObjectPool"%>
<%@page import="mvc.model.server.DemoServer"%>
<%@page import="mvc.controller.ServletProxy"%>

<%

    String siteUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
    //
    ServletProxy sp = new ServletProxy(siteUrl);
    String serverButtons = sp.formatJsonUrisOfTypeAsButtons("Server");
    String cracButtons = sp.formatJsonUrisOfTypeAsButtons("CRAC");
    //
    ModelObjectPool pool = ModelObjectPool.getInstance();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP</title>
    </head>
    <body>
        <h2>Dynamic site root URL generation</h2>
        <div>
            Site URL is <a href="<%=siteUrl%>"><%=siteUrl%></a>
        </div>

        <div>
            <h2>Buttons retrieved via <a href="<%=siteUrl%>src/java/mvc/controller/ServletProxy.java">ServletProxy</a></h2>
            <div>
                <div>
                    <h3>Server buttons</h3>
                    <%=serverButtons%>
                    <div id="div2">
                        <p>This is div2</p>
                    </div>
                    <h3>CRAC buttons</h3>
                    <div>
                        <%=cracButtons%>
                    </div>
                </div>
            </div>
        </div>


        <div>
            <h2>Check of how many <a href="<%=siteUrl%>src/java/mvc/model/ModelObjectInterface.java">ModelObjectInterface</a> implementing objects are in <a href="<%=siteUrl%>src/java/controller/modelobject/ModelObjectPool.java">ModelObjectPool</a></h2>
            ModelObjectPool contains <%=pool.getModelObjectList().size()%> objects.
        </div>

        <div>
            <h2>Debugging log</h2>
            <p><%=sp.getLog()%></p>

            <p><a href="test_pages/AJAXRecipientTest.html">AJAX Recipient Test page</a></p>
    </body>
</html>