<%-- 
    Document   : AddObjectForm_V2
    Created on : 07-Aug-2015, 15:37:52
    Author     : Daniel Pawsey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="utils.AddObjectForm_Controller"%>
<%
    AddObjectForm_Controller controller = new AddObjectForm_Controller();
    String typeDropdown = controller.getTablesAsDropdownList();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/AddObjectForm.css">
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
        <script src="js/utils/UploadFormUtils.js"></script>
        <title>Add object form | Data Centre Modelling</title>
    </head>

    <body>
        <form action="AddObjectServlet" method="POST" enctype="multipart/form-data" name="myForm" ng-app>

            <div class="failMessage">
                <h3>PLEASE NOTE</h3>
                <p>Do not include spaces in the names of objects you are adding.</p>
                <p>Use either underscores, or camel-script</p>
                <p>For example, rather than calling an object 'Air Conditioner', 
                    call it 'Air_Conditioner' or 'AirConditioner'.</p>
            </div>

            Object type: <%=typeDropdown%><br>
            Object name: <input type="text" name="name" ng-model="fieldCheck.name"/>
            <br/>
            Power consumption in kWh: <input type="number" name="kWh" ng-model="fieldCheck.kWh"/>
            <br/>
            Carbon Dioxide output: <input type="number" name="co2" ng-model="fieldCheck.co2"/>
            <br/>
            Temperature addition to data centre: <input type="number" name="temp" ng-model="fieldCheck.temp"/>
            <br/>
            Maximum operating temperature: <input type="number" name="tempThreshold" ng-model="fieldCheck.tempThreshold"/>

            <p>When finished, please upload a three.js formatted JSON file (.js file) to continue:</p>
            <div id="yourBtn" class="controlButton" onclick="getFile()">click to upload a file</div>
            <div class="hiddenInputWrapper">
                <input id="upfile" type="file" value="upload" name="jsonModel" onchange="sub(this)" ng-disabled="!(!!fieldCheck.name && !!fieldCheck.kWh && !!fieldCheck.co2 && !!fieldCheck.temp && !!fieldCheck.tempThreshold)"/>
            </div>
        </form>
    </body>
</html>