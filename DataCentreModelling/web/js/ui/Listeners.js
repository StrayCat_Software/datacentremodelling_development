/*
 * This class adds listeners to the scene, allowing mouse and keyboard 
 * interaction. All functionality is added to the calling class by this class's
 * constructor.
 * 
 * This approach to object-oriented programming in JavaScript is derived from 
 * code found at 
 * https://www.scorchsoft.com/blog/how-to-write-object-oriented-javascript/
 * 
 * @author Daniel Pawsey
 * @returns {Listeners}
 */
var Listeners = function () {
    
    /**
     * @constructor adds mouse movement and window resize listeners to the page.
     */
    this.constructor = function () {
        document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('mousedown', onDocumentMouseDown, false);
        window.addEventListener('resize', onWindowResize, false);
    };
    this.constructor();
};