/**
 * This class models the skybox.
 * 
 * This approach to object-oriented programming in JavaScript is derived from 
 * code found at 
 * https://www.scorchsoft.com/blog/how-to-write-object-oriented-javascript/
 * 
 * @author Daniel Pawsey
 * @returns {Skybox}
 */
var Skybox = function () {
    
    /**
     * @type THREE.CubeGeometry
     */
    var skyboxGeometry = new THREE.CubeGeometry(100000, 100000, 100000);
    
    /**
     * @type THREE.MeshBasicMaterial
     */
    var skyboxMaterial = new THREE.MeshBasicMaterial(
            {color: 0x7ec0ee, side: THREE.BackSide});
    /**
     * 
     * @type THREE.Mesh
     */
    var skybox = new THREE.Mesh(skyboxGeometry, skyboxMaterial);

    /*
     * Draw the skybox.
     * 
     * @returns {undefined}
     */
    Skybox.prototype.draw = function () {
        scene.add(skybox);
    };
};