/**
 * This class construcs the camera and the user controls, and adds them to the
 * scene. It also has methods for rapidly reorienting the view with button
 * presses.
 * 
 * This approach to object-oriented programming in JavaScript is derived from 
 * code found at 
 * https://www.scorchsoft.com/blog/how-to-write-object-oriented-javascript/
 * 
 * @author Daniel Pawsey
 * @returns {CameraControls}
 */
var CameraControls = function () {

    /**
     * @constructor
     */
    this.constructor = function () {
        camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 1000000);
        camera.position.set(500, 800, 1300);
        controls = new THREE.OrbitControls(camera);
        controls.noZoom = false;
        controls.noPan = false;
        controls.staticMoving = true;
        controls.maxPolarAngle = Math.PI / 2;
        controls.maxDistance = 8000;
        controls.addEventListener('change', render);
    };
    
    /**
     * Reset the camera orientation to it's original position.
     * @returns {undefined}
     */
    this.reset = function () {
        camera.position.set(500, 800, 1300);
    };
    
    /**
     * Reorient the camera to a side-on view.
     * @returns {undefined}
     */
    this.sideView = function () {
        camera.position.set(0, 0, 1300);
    };
    
    /**
     * Reorient the camera to a top-down view.
     * @returns {undefined}
     */
    this.topView = function () {
        camera.position.set(0, 1500, 50);
    };

    /**
     * Automated call to constructor means that all class functionality takes
     * place on construction.
     */
    this.constructor();
};