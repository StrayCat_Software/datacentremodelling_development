/*
 * This class models the rollover helper to the scene, and has a function which 
 * draws it.
 * 
 * This approach to object-oriented programming in JavaScript is derived from 
 * code found at 
 * https://www.scorchsoft.com/blog/how-to-write-object-oriented-javascript/
 * 
 * @author Daniel Pawsey
 * @returns {RolloverCursor}
 */
var RolloverCursor = function () {

    /*
     * @constructor
     */
    this.construct = function () {
        // Starts off with a simple box for the cursor, until a object is 
        // chosen.
        rollOverGeo = new THREE.BoxGeometry(150, 100, 100);
    };

    /*
     * Draws the cursor as a red translucent cuboid.
     *  
     * @returns {undefined}
     */
    RolloverCursor.prototype.draw = function () {

        rollOverMaterial = new THREE.MeshBasicMaterial(
                {color: 0xff0000, opacity: 0.5, transparent: true});
        rollOverMesh = new THREE.Mesh(rollOverGeo, rollOverMaterial);
        scene.add(rollOverMesh);
    }

    /**
     * Sets the cursor to be a translucent green version of the selected object.
     * 
     * @param {type} jsonString The data of the model that will be drawn.
     * @param {type} angle The angle to rotate the object to.
     * @returns {undefined}
     */
    RolloverCursor.prototype.setModel = function (jsonString, angle) {
        var loader = new THREE.JSONLoader();
        var newGeo = loader.parse($.parseJSON(jsonString));
        scene.remove(rollOverMesh);
        newMaterial = new THREE.MeshBasicMaterial(
                {color: 0x00FF00, opacity: 0.5, transparent: true});
        rollOverMesh = new THREE.Mesh(newGeo.geometry, newMaterial);
        rollOverMesh.scale.set(30, 30, 30);

        // Object rotation
        this.ang = function (degree) {
            return degree * (Math.PI / 180);
        };
        
        rollOverMesh.rotation.y = this.ang(angle);
        scene.add(rollOverMesh);
    };
    
    RolloverCursor.prototype.rotateCursor = function (angle) {
        scene.remove(rollOverMesh);
        // Object rotation
        this.ang = function (degree) {
            return degree * (Math.PI / 180);
        };
        
        rollOverMesh.rotation.y = this.ang(angle);

        scene.add(rollOverMesh);
    };

    /*
     * Constructor is always called, forcing any code included there to be
     * executed.
     */
    this.construct();
};