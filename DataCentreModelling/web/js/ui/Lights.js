/*
 * Create and place scee ambient and directional lighting.
 * 
 * This approach to object-oriented programming in JavaScript is derived from 
 * code found at 
 * https://www.scorchsoft.com/blog/how-to-write-object-oriented-javascript/
 * 
 * @author Daniel Pawsey
 * @returns {Lights}
 */
var Lights = function () {
    
    /**
     * 
     * @type THREE.DirectionalLight|THREE.AmbientLight
     */
    var light;
    /**
     * Add the lights to the scene.
     * 
     * @returns {undefined}
     */
    Lights.prototype.draw = function () {
        light = new THREE.DirectionalLight(0xffffff);
        light.position.set(1, 1, 1);
        scene.add(light);
        light = new THREE.AmbientLight(0x222222);
        scene.add(light);
    };
};