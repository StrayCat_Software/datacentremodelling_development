/*
 * This class represents a data-centre floor, drawn as a rollover mesh so that 
 * the class RolloverCursor can correctly interact with it.
 * 
 * This approach to object-oriented programming in JavaScript is derived from 
 * code found at 
 * https://www.scorchsoft.com/blog/how-to-write-object-oriented-javascript/
 * 
 * @author Daniel Pawsey
 * @returns {Grid}
 */
var Grid = function () {

    /**
     * 
     * @type Number
     */
    var size;
    /**
     * 
     * @type THREE.Geometry
     */
    var geometry;
    /**
     * 
     * @type THREE.LineBasicMaterial
     */
    var material;
    /**
     * 
     * @type THREE.Line
     */
    var line;

    var levelArray = new Array();

    var levelNumber;

    var buildingheight = 0;

    /*
     * Blank constructor, included for migratory purposes
     * 
     * @returns {undefined}
     */
    this.construct = function (/*Anything passed to the class*/) {
        //TODO constructor code here.
    }

    /**
     * Add the floors in their correct positions to the scene.
     * 
     * @param {type} levelsNum The number of floors to draw.
     * 
     * @param {type} ceilingHeight The space between floors.
     * 
     * @param {type} floorSpace The square size of the floors.
     * 
     * @returns {undefined}
     */
    Grid.prototype.makeLevels = function (levelsNum, ceilingHeight, floorSpace) {
        this.levelNumber = levelsNum;
        for (var i = 0; i <= levelsNum; i++) {
            drawGrid(i * ceilingHeight, floorSpace);
            drawPlane(i * ceilingHeight, floorSpace);
            this.makeBuilding(ceilingHeight, floorSpace, i);
            //building.position.y = ceilingHeight;

        }
    }

    /**
     * Draw a PlaneBufferGeometry object to represent the groud beneth the 
     * lowest floor.
     * 
     * @returns {undefined}
     */
    Grid.prototype.makeGround = function () {
        var geo = new THREE.PlaneBufferGeometry(100000, 100000);
        geo.applyMatrix(new THREE.Matrix4().makeRotationX(-Math.PI / 2));
        var material = new THREE.MeshBasicMaterial({color: 0x01A611, side: THREE.DoubleSide});
        var ground = new THREE.Mesh(geo, material);
        ground.position.y = -50;
        scene.add(ground);
    }

    /**
     * Draw the walls and ceiling around the floors as a blue transparant box.
     * 
     * @param {type} ceilingHeight The space above each floor. It is at this 
     * height above the highest floor that the ceiling will be drawn.
     * 
     * @param {type} floorSpace The square space of the bottom and top of the 
     * box.
     * 
     * @param {type} count The number of floors in the data centre. Used to 
     * calculate the height of the box as the the ceilingHeight multiplied by 
     * the count.
     * 
     * @returns {undefined}
     */
    Grid.prototype.makeBuilding = function (ceilingHeight, floorSpace, count) {
        var area = floorSpace * 2;
        var height = ceilingHeight * 2;
        var skyboxGeometry = new THREE.CubeGeometry(area, height, area);
        var buildingMaterial = new THREE.MeshBasicMaterial(
                {color: 0x7ec0ee, opacity: 0.2, transparent: true, side: THREE.DoubleSide, });
        var level = new THREE.Mesh(skyboxGeometry, buildingMaterial);
//        level.position.y = buildingheight;
        level.position.y = ceilingHeight * count;
        buildingheight = buildingheight + ceilingHeight;
        levelArray.push(level);
        scene.add(level);
    }

    /**
     * Draw the building around the floors.
     * 
     * @returns {undefined}
     */
    Grid.prototype.addBuilding = function () {
//        scene.add(building);
        for (var i = 0; i <= levelArray.length - 1; i++) {
            scene.add(levelArray[i]);
        }
    }

    /**
     * Remove the building from around the floors.
     * 
     * @returns {undefined}
     */
    Grid.prototype.removeBuiding = function () {
//        scene.remove(building);

        for (var i = levelArray.length - 1; i >= 0; i--) {
            scene.remove(levelArray[i]);
        }
    }

    /**
     * Draw the visible grid, marking the location of the plane drawn by
     * this.drawPlane().
     * 
     * @returns {undefined}
     */
    function drawGrid(groundHeight, floorSpace) {

        size = floorSpace, step = 50;
        geometry = new THREE.Geometry();

        for (var i = -size; i <= size; i += step) {

            geometry.vertices.push(new THREE.Vector3(-size, 0, i));
            geometry.vertices.push(new THREE.Vector3(size, 0, i));

            geometry.vertices.push(new THREE.Vector3(i, 0, -size));
            geometry.vertices.push(new THREE.Vector3(i, 0, size));

        }
        material = new THREE.LineBasicMaterial({color: 0x000000, opacity: 0.2, transparent: true});
        line = new THREE.Line(geometry, material, THREE.LinePieces);
        line.position.y = groundHeight;
        scene.add(line);

    }

    /**
     * This draws the plane on which the rollover cursor and objects will 
     * be drawn. It is invisible, but occupies the same space as 
     * this.drawGrid().
     * 
     * @param {type} The y-axis position at which to draw the floor.
     * @param {type} floorSpace The area of the floor, to be squared.
     * @returns {undefined}
     */
    function drawPlane(groundHeight, floorSpace) {
        var area = floorSpace * 2;
        var geometry = new THREE.PlaneBufferGeometry(area, area);
        geometry.applyMatrix(new THREE.Matrix4().makeRotationX(-Math.PI / 2));
        var material = new THREE.MeshPhongMaterial({color: 0xffffff, specular: 0x555555, shininess: 100000, side: THREE.DoubleSide});
        var plane = new THREE.Mesh(geometry, material);
        plane.position.y = groundHeight;
        scene.add(plane);

        objects.push(plane);
    }

    Grid.prototype.makeBuildingCeiling = function () {

    }

    /*
     * Constructor is always called, forcing any code included there to be
     * executed.
     */
    this.construct();
    this.makeGround();
};