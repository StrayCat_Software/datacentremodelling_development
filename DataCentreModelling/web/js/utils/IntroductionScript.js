/**
 * Singleton class, called to automatically display introductory session help 
 * pannels.
 * 
 * Requires jQuery.
 * 
 * Adapted from code found at 
 * http://www.dofactory.com/javascript/singleton-design-pattern
 * 
 * @author Daniel Pawsey
 * @since 27/07/2015
 * @returns {IntroductionScript}
 */
var IntroductionScript = (function () {
    var instance;
    
    /**
     * The stage is the number to use when calling the page component to 
     * display in index.html's centreBox from HelpPage.html, as the relevant 
     * page components on that page are identified by number.
     * 
     * @type Number
     */
    var stage = 0;

    function createInstance() {
        var object = new Object("I am the instance");
        runScript();
        return object;
    }

    /**
     * Displays the next available help item on HelpPage.html.
     * 
     * @returns {undefined}
     */
    function runScript() {
        $('#centreBoxContent').show().empty().load('HelpPage.html #' + stage, function () {

            // If the required page component is not empty, add the continue button to the top bar,
            // otherwise hide and empty it.
            var resultCheck = $('#' + stage).text();
            if (resultCheck !== "") {
                $('#boxTopBar').show().empty().append(
                        '<div id="boxTopBarContent"><button class="controlButton" onclick="clearCentreBox()">Done</button>'
                        + '<button class="controlButton" onclick="introductionNext()">Continue</button></div>');
            }
            else {
                $('#boxTopBar').hide().empty();
            }
            
            stage = stage + 1;
        });


    };
    
    // Return an instance of this singleton.
    return {
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            else {
                runScript();
            }
            return instance;
        }
    };
})();

/**
 * Called by the continue button supplied with the introductory panels.
 * 
 * @returns {undefined}
 */
function introductionNext() {
    var introductionScript = IntroductionScript.getInstance();
}