/**
 * Page load scripts, including initialisers for AJAX requests from 
 * VCSynapse.jsp, calls to create object buttons, and local storage checks for 
 * prior visitation by the user.
 * 
 * @returns {undefined}
 */
function pagePrimer() {
    $(document).ready(function() {
        var utils = new AJAXUtils();
        utils.setHost('VCSynapse.jsp');
        utils.makeButtons('objectSelectionButtons', 'objectSelectionButtons');
        var cookieChecker = new CookieChecker();
        cookieChecker.checkForCookie();
        addBuilding();
    });
}

/**
 * Empties the centre box, populates it with content from LoginForm.html via 
 * AJAX requests and makes it visible.
 * 
 * @returns {undefined}
 */
function loginDialog() {
    $('#centreBoxContent').empty().show().load('LoginPage.html #loginForm');
}

function loginForm() {
    $('#pageContentArea').empty();
    $('#pageContentArea').append('<iframe src="LoginPage.html" frameborder="0"></iframe>');
}

/**
 * Hides the content in the centre box.
 * @returns {undefined}
 */
function clearCentreBox() {
    $('#centreBoxContent').hide();
    $('#boxTopBar').hide()
}

/**
 * Empties the centre box, populates it with content from HelpPage.html via 
 * AJAX requests and makes it visible.
 * 
 * @returns {undefined}
 */
function help() {
    $('#centreBoxContent').show().empty().load('HelpPage.html #helpPageContent', function() {
        $('#0').hide();
    });
    $('#boxTopBar').show().empty().append('<div id="boxTopBarContent"><button class="controlButton" onclick="clearCentreBox()">Done</button></div>');
}

function adjustLayout(){
    $('#centreBoxContent').empty().show().load('EnvironmentResizer.html #envModifier');
}

/**
 * Hides the side panel, then replaces the "Hide" button with a "Show" button.
 * 
 * @returns {undefined}
 */
function hideSidePanel() {
    $('#sidePanelContent').hide();
    $('#sidePannelToggleButton').empty();
    $('#sidePannelToggleButton').append('<button class="controlButton drawerButton" onclick="showSidePanel()">+</button>');
}

/**
 * Makes the side panel visible, then replaces the "Show" button with a "Hide" 
 * button.
 * 
 * @returns {undefined}
 */
function showSidePanel() {
    $('#sidePanelContent').show();
    $('#sidePannelToggleButton').empty();
    $('#sidePannelToggleButton').append('<button class="controlButton drawerButton" onclick="hideSidePanel()">-</button>');
}

/**
 * Hides the bottom panel, then replaces the "Hide" button with a "Show" button.
 * 
 * @returns {undefined}
 */
function hideBottomPanel() {
    $('#bottomBarContent').hide();
    $('#bottomPanelToggleButton').empty();
    $('#bottomPanelToggleButton').append('<button class="controlButton drawerButton" onclick="showBottomPanel()">+</button>');
}

/**
 * Makes the side panel visible, then replaces the "Show" button with a "Hide" 
 * button.
 * 
 * @returns {undefined}
 */
function showBottomPanel() {
    $('#bottomBarContent').show();
    $('#bottomPanelToggleButton').empty();
    $('#bottomPanelToggleButton').append('<button class="controlButton drawerButton" onclick="hideBottomPanel()">-</button>');
}

/**
 * Calls a form as an iframe from a specified page. iframe is  used to allow 
 * the option of using AngularJS form validation with the form on the calling 
 * page.
 * 
 * @param {type} formPage The page to call the form from.
 * @returns {undefined}
 */
function loadForm(formPage) {
    $('#pageContentArea').empty();
    $('#pageContentArea').append('<iframe src="' + formPage + '.jsp" frameborder="0"></iframe>');
}