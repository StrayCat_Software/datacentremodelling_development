/**
 * This contains utility methods for handling AJAX data.
 * 
 * @returns {AJAXUtils}
 * @author Daniel Pawsey
 * @since 08/06/2015
 * @version 1.0
 */
var AJAXUtils = function () {

    var totalCo2;

    /**
     * A container with setters and getters for the string to be supplied to 
     * "hostPage_String".
     * @type StringContainer
     */
    var hostPage = new StringContainer();

    /**
     * A reference to the file from which the AJAX data will be retrieved.
     */
    var hostPage_String = "";

    /**
     * Sets "hostPage_String".
     * 
     * @param {type} page The file-name of the page from which the AJAX data 
     * will be called. This is to be supplied as just the file-name and it's 
     * extention, not it's full path. For example, to call MyPage.html, the 
     * correct parameter to provide would be "MyPage.html".
     * 
     * @returns {undefined}
     */
    this.setHost = function (page) {
        hostPage.setString(page);
        hostPage_String = hostPage.getString();
    };

    /**
     * Produces a button on page by making an AJAX connection between the 
     * calling page and the content hosting page.
     * 
     * @param {type} divId The id attribute of the div tag from which the data 
     * is to be retrieved on the host page, and of the div tag to which the 
     * data is to be passed on the recipient page. These must match on both 
     * pages.
     * 
     * @param {type} recipient_divId
     * @param {type} host_divId
     * @returns {undefined}
     */
    this.makeButtons = function (recipient_divId, host_divId) {
        $("#" + recipient_divId).load(hostPage_String + " #" + host_divId);
    };
};