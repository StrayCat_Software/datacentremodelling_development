/**
 * This object is a container for a String reference to a web-page. It includes 
 * accessor methods for that String.
 * 
 * @returns {HostPage}
 * @author Daniel Pawsey
 * @version 1.0
 * @since 08/06/2015
 */
var StringContainer = function() {
    
    /**
     * The name, set and called as a String.
     * 
     * @type String
     */
    var name = "";
    
    /**
     * Sets "page".
     * 
     * @param {type} page The file-name of the page to reference. This must be 
     * the whole file name of the page to be referenced, e.g. "MyPage.html". 
     * It should NOT be the full-path of the file.
     * 
     * @returns {undefined}
     */
    this.setString = function(name) {
        this.name = name;
    };
    
    /**
     * Calls "page"
     * 
     * @returns {type|String}
     */
    this.getString = function() {
        return this.name;
    };
};