/**
 * This class is part of the first-time introduction feature. It will check the
 * client's JavaScript LocalStorage for a boolean item with the key 
 * "priorVisitor" with the value false: if not found, it will call an instance
 * of the IntroductionScript singleton class. 
 * 
 * @author Daniel Pawsey
 * @since 27/07/2015
 * @returns {CookieChecker}
 */
var CookieChecker = function () {
    
    /**
     * Check LocalStorage for the "priorVisitor" item. If not found, call 
     * CookieChecker.addCookie.
     * 
     * @returns {undefined}
     */
    CookieChecker.prototype.checkForCookie = function(){
        if(!localStorage.getItem("priorVisitor")){
            this.addCookie();
        }
    };
    
    /**
     * Calls an instance of IntroductionScript.
     * 
     * @returns {undefined}
     */
    CookieChecker.prototype.addCookie = function(){
        localStorage.setItem("priorVisitor", true);
        var introductionScript = IntroductionScript.getInstance();
    };
};