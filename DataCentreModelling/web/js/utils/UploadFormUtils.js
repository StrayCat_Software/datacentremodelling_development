// With code found at 
// https://stackoverflow.com/questions/4909228/style-input-type-file

/**
 * Retrieves file content written into a file input hidden within a page div in 
 * the uploader form on AddObjectForm.jsp when that div is clicked upon.
 * 
 * @returns {undefined}
 */
function getFile() {
    document.getElementById("upfile").click();
}

/**
 * Retrieves the file from and submits the form on AddObjectForm.jsp. This 
 * leads to a servlet call, which then sends the user to a generated 
 * confirmation page.
 * 
 * @param {type} obj
 * @returns {undefined}
 */
function sub(obj) {
    var file = obj.value;
    var fileName = file.split("\\");
    document.getElementById("yourBtn").innerHTML = fileName[fileName.length - 1];
    document.myForm.submit();
    event.preventDefault();
}
