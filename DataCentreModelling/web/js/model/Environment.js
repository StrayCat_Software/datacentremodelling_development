/**
 * This class handles all 3D visual environment creation. It is called by on 
 * index.html loading via an onload call to the JavaScript file 
 * DataCentreModelling_Controller.js.
 * 
 * @author Daniel Pawsey
 * 
 * @returns {Environment}
 */
var Environment = function () {

    /*
     * Camera with trackball controls. Note that CameraControls's 
     * constructor  fully initialises it, and not other methods need to be 
     * called.
     * 
     * @type CameraControls
     */
    var cam = new CameraControls();

    /*
     * Draw the grid-plane.
     * 
     * @type Grid
     */
    var grid = new Grid();

    /*
     * Draw the rollover cursor box.
     * 
     * @type RolloverCursor
     */
    var cursor = new RolloverCursor();

    /*
     * Draw the skybox.
     * 
     * @type Skybox
     */
    var skybox = new Skybox();

    /*
     * Create and place the lights.
     * 
     * @type Lights
     */
    var lights = new Lights();

    /*
     * Render the scene.
     * 
     * @type Renderer
     */
    var rend = new Renderer();

    /*
     * Add listeners to the scene. All functionality is handled by the 
     * class's constructor.
     * 
     * @type Listeners
     */
    var listeners = new Listeners();

    /**
     * Passes the values used to draw the floors to the Grid class.
     * 
     * @param {type} floorNum The number of floors to draw.
     * 
     * @param {type} ceilingHeight The y-axis space between the floors to be 
     * added.
     * 
     * @param {type} floorSpace The square area of the floor to be drawn.
     * @returns {undefined}
     */
    Environment.prototype.setFloors = function (floorNum, ceilingHeight, floorSpace) {
        grid.makeLevels(floorNum - 1, ceilingHeight, floorSpace);
    }

    /**
     * <p>This constructor creates the visual 3D environment by constructing all
     * of the required classes, and calling their required methods.</p>
     * 
     * @constructor
     * 
     * @returns {undefined}
     */
    Environment.prototype.constructor = function () {
        cursor.draw();
        skybox.draw();
        lights.draw();
        rend.render();
    };

    /**
     * This method allows the passage of newly selected JSON models from 
     * {@link js/controller/DataCentreModelling_Controller} to 
     * {@link RolloverCursor#setModel}.
     * 
     * @param {type} jsonString The pre parsed three.js JSON data, as a string.
     * 
     * @returns {undefined}
     */
    Environment.prototype.setCursor = function (jsonString, angle) {
        cursor.setModel(jsonString, angle);
    };

    /**
     * Changes the y-angle axis the rollover-cursor will be re-drawn at via 
     * calling RolloverCursor's rotateCursor method.
     * 
     * @param {type} angle The angle to which to rotate the cursor
     * @returns {undefined}
     */
    Environment.prototype.rotateCursor = function (angle) {
        cursor.rotateCursor(angle);
    };

    /**
     * Draws the ground as a three.js PlaneBufferGeometry, 100 units below 0 
     * (where the floor is drawn) and coloured green.
     * 
     * @returns {undefined}
     */
    Environment.prototype.drawGround = function () {
        var geometry = new THREE.PlaneBufferGeometry(10000, 10000);
        geometry.applyMatrix(new THREE.Matrix4().makeRotationX(-Math.PI / 2));
        plane = new THREE.Mesh(geometry);
        plane.position.x = -100;
        scene.add(plane);

        objects.push(plane);
    };

    /**
     * Draws a blue transparant box around the data-centre to represent where 
     * the walls and ceiling would be.
     * 
     * @param {type} addWalls Whether or not to draw the walls.
     * 
     * @returns {undefined}
     */
    Environment.prototype.setWalls = function (addWalls) {
        if (addWalls === true) {
            grid.addBuilding();
        }
        else {
            grid.removeBuiding();
        }
    };

    this.constructor();
};