/**
 * <p>This factory class is called by the controller class as a global object. 
 * It functions as a singleton class.</p>
 * 
 * <p>It is uses specialised JSON files from the web/resources/JSON folder to 
 * draw 3D objects, and add them to the controller class's scene.</p>
 * 
 * @author Daniel Pawsey
 * 
 * @param {type} scene
 *      The Scene on which the objects will be drawn.
 * @param {type} objects
 *      The list of 3D objects already associated with the Scene.
 * @returns {ObjectMetricController}
 */
var ObjectMetricController = function (scene, objects) {

    /**
     * The scene to be rendered.
     */
    var scene = scene;
    /**
     * The list of 3D objects.
     */
    var objects = objects;


    /**
     * The sum total of the value which corresponds to the co2 attribute 
     * the corresponding object in the Java model.
     */
    var co2MetricNumber = 0;

    /**
     *  The sum total of the value which corresponds to the kWh attribute 
     * the corresponding object in the Java model.
     */
    var kWhMetricNumber = 0;

    /**
     * The sum of all temp attributes of all objects added to the environment.
     */
    var tempMetricNumber = 0;

    /**
     * A list of temperature at which at least one object in the environment 
     * will overheat if exceeded.
     */
    var tempThresholdArray = new Array();

    var lastObject;

    var thresholdExceeded = false;

    var lightColourArray = new Array();

    var minThreshold;

    /**
     * This method draws the 3D object from the JSON file indicated by the 
     * supplied URL string, at the position indicated by the supplied 
     * intersect.
     * 
     * @param {type} objectURL
     *      A string indicating the URL of the JSON object used to draw the 3D
     *      object.
     * @param {type} intersect
     *      The position at which the 3D object will be drawn.
     * @returns {undefined}
     */
    ObjectMetricController.prototype.drawObject = function (objectURL, intersect) {
        // instantiate a loader
        var loader = new THREE.JSONLoader();
        // load a resource
        loader.load(
                //resource URL
                objectURL,
                // Function when resource is loaded
                        function (geometry, materials) {
                            var material = new THREE.MeshFaceMaterial(materials);
                            var object = new THREE.Mesh(geometry, material);
                            object.position.copy(intersect.point).add(intersect.face.normal);
                            object.position.divideScalar(50).floor().multiplyScalar(50).addScalar(25);
                            scene.add(object);
                            objects.push(object);
                            lastObject = object;
                        }
                );
            };

    /**
     * Adds the value of the specified attribute for the specified object to 
     * the running total for that attribute, and displays the new value in it's 
     * appropriate <code>div</code> tag.
     * 
     * These values are fetched from VCSynapse.jsp using a jQuery
     *  <code>load<code> query, which is constructed by concatenating the 
     *  object and attribute names as the page component value to retrieve, 
     *  resulting in a load query like 'VCSynapse.jsp #ConcreteServerCo2'.
     * 
     * @param {type} objectClassName The name of the object of the attribute to 
     * be retrieved from VCSynapse.jsp. This is to be supplied as a String 
     * value corresponding to the name of a class in the Java model, such as 
     * 'ConcreteServer'.
     * 
     * @param {type} metricType The name of the attribute to be fetched from 
     * VCSynapse.jsp for the supplied object. This must have a capitalised 
     * first letter, so the kWh attribute of the ConcreteServer object would be 
     * supplied to this parameter as KWh. This is because the attributes are 
     * written in CamelScript on VCSynapse.jsp, and the attribute name follows 
     * the object name for each page component; so ConcreteServer's kWh value 
     * would be written into a <code>div</code> tag as 
     * '<code>ConcreteServerKWh</code>'.
     * 
     * @returns {undefined} Does not return a value: instead empties the calling
     * page's relevant tag, and appends the new value into it by using jQuery
     * <code>empty</code> and <code>append</code> operations.
     */
    ObjectMetricController.prototype.getMetric = function (objectClassName, metricType) {

        if (!localStorage.getItem(objectClassName + metricType)) {
            // Retrieve the value of the metric of the selected object, and parse it to a global float.
            $('#' + metricType + 'Zero').hide().load('VCSynapse.jsp #' + objectClassName + metricType, function (result) {
                // Retrieve the page component as a String.
                var addMetricString = $('#' + objectClassName + metricType).text();

                // Assign the retrieved value to a global float variable.
                addAmbiguousMetricNumber = 0;
                addAmbiguousMetricNumber = parseFloat(addMetricString);

                // Append the new data into the correct page component.
                $('#' + metricType + 'Metric').empty();

                // Add the retrieved metric to the total count for that metric.
                if (metricType === 'KWh') {
                    kWhMetricNumber += addAmbiguousMetricNumber;
                    $('#KWhMetric').append("Power consumption: <span class=\"metricData\">" + kWhMetricNumber + "kWh</span>");
                    localStorage.setItem(objectClassName + metricType, addAmbiguousMetricNumber);
                }
                if (metricType === 'Co2') {
                    co2MetricNumber += addAmbiguousMetricNumber;
                    $('#Co2Metric').append("Carbon dioxide output: <span class=\"metricData\">" + co2MetricNumber + " Tonnes per year</span>");
                    localStorage.setItem(objectClassName + metricType, addAmbiguousMetricNumber);
                }
                if (metricType === 'Temp') {
                    tempMetricNumber += addAmbiguousMetricNumber;
                    $('#TempMetric').append("Current data centre temperature: <span class=\"metricData\">" + tempMetricNumber + "&deg;C</span>");
                    localStorage.setItem(objectClassName + metricType, addAmbiguousMetricNumber);
                }
            });
        }
        else {
            var num = 0;
            num = parseFloat(localStorage.getItem(objectClassName + metricType));

            $('#' + metricType + 'Metric').empty();

            if (metricType === 'KWh') {
                kWhMetricNumber += num;
                $('#KWhMetric').append("Power consumption: <span class=\"metricData\">" + kWhMetricNumber + "kWh</span>");
            }
            if (metricType === 'Co2') {
                co2MetricNumber += num;
                $('#Co2Metric').append("Carbon dioxide output: <span class=\"metricData\">" + co2MetricNumber + " Tonnes per year</span>");
            }
            if (metricType === 'Temp') {
                tempMetricNumber += num;
                $('#TempMetric').append("Current data centre temperature: <span class=\"metricData\">" + tempMetricNumber + "&deg;C</span>");
            }
        }
    };

    /**
     * Gets the tempThreshold value for the current object, adds it to 
     * tempThresholdArray, finds the lowest value in tempThresholdArray and 
     * appends that to a page componenet. If the overall temp for the 
     * application is higher than the threshold, a warning message is posted 
     * on the page.
     * 
     * @param {type} objectClassName The object to be checked for it's 
     * tempThreshold value.
     * 
     * @returns {undefined}
     */
    ObjectMetricController.prototype.getLowestTempThreshold = function (objectClassName) {

        // Retrieve the value of the tempThreshold of the selected object, and 
        // parse it to a global float.
        $('#TempThresholdZero').hide().load('VCSynapse.jsp #' + objectClassName + 'TempThreshold', function () {
            // Retrieve the page component as a String.
            var thresholdString = $('#' + objectClassName + 'TempThreshold').text();
            var threshold = parseFloat(thresholdString);

            tempThresholdArray[tempThresholdArray.length] = threshold;

            // Check for the lowest tempThreshold, and set that as the actual
            // temp threshold value for the application.
            var checkThreshold;

            for (var i = 0; i <= tempThresholdArray.length; i++) {

                // If there is no current minimum threshold value, assign it 
                // as the current threshold value.
                if (i <= 0) {
                    minThreshold = tempThresholdArray[i];
                }

                // Check whether the current threshold value is less than the
                // minimum theshold value, and reassign it.
                checkThreshold = tempThresholdArray[i];
                if (checkThreshold < minThreshold) {
                    minThreshold = checkThreshold;
                }
            }

            // If the temperiture is greater than the minThreshold, display a
            // notice on the page.
            if (tempMetricNumber > minThreshold) {
                $('#thresholdWarningNotice').empty().append('<div class=\"failMessage\"><p>Warning: Temperiture threshold exceeded!</p></div>');
                thresholdExceeded = true;
            }


            // Display the minimum temp threshold on the page.
            $('#tempThreshold').empty();
            $('#tempThreshold').append("Data centre current maximum temperature: <span class=\"metricData\">" + minThreshold + "&deg;C</span>");
        });
    };

    ObjectMetricController.prototype.getThresholdExceeded = function () {
        return thresholdExceeded;
    };

    /**
     * Removes the kWh, CO2 and threshold data for the last added 3D object.
     * 
     * @param {type} objectClassName
     * @param {type} metricType
     * @returns {undefined}
     */
    ObjectMetricController.prototype.removeData = function (objectClassName, metricType) {

        if (!localStorage.getItem(objectClassName + metricType)) {
            // Retrieve the value of the metric of the selected object, and parse it to a global float.
            $('#' + metricType + 'Zero').hide().load('VCSynapse.jsp #' + objectClassName + metricType, function (result) {

                // Retrieve the page component as a String.
                var addMetricString = $('#' + objectClassName + metricType).text();

                // Assign the retrieved value to a global float variable.
                subtractAmbiguousMetricNumber = 0;
                subtractAmbiguousMetricNumber = parseFloat(addMetricString);
                localStorage.setItem(objectClassName + metricType, subtractAmbiguousMetricNumber);

                // Append the new data into the correct page component.
                $('#' + metricType + 'Metric').empty();

                // Add the retrieved metric to the total count for that metric.
                if (metricType === 'KWh') {
                    kWhMetricNumber = kWhMetricNumber - subtractAmbiguousMetricNumber;
                    // Bugfix: kWh should never be below 0. If it does fall below 0, set it to 0.
                    if (kWhMetricNumber < 0) {
                        kWhMetricNumber = 0;
                    }
                    $('#KWhMetric').append("Power consumption: <span class=\"metricData\">" + kWhMetricNumber + "kWh</span>");
                }
                if (metricType === 'Co2') {
                    co2MetricNumber = co2MetricNumber - subtractAmbiguousMetricNumber;
                    // Bugfix: co2 should never be below 0. If it does fall below 0, set it to 0.
                    if (co2MetricNumber < 0) {
                        co2MetricNumber = 0;
                    }
                    $('#Co2Metric').append("Carbon dioxide output: <span class=\"metricData\">" + co2MetricNumber + " Tonnes per year</span>");
                }
                if (metricType === 'Temp') {
                    tempMetricNumber = tempMetricNumber - subtractAmbiguousMetricNumber;
                    $('#TempMetric').append("Current data centre temperature: <span class=\"metricData\">" + tempMetricNumber + "&deg;C</span>");

                    // If the temp has fallen belowe the threshold, remove the 
                    // warning message and replace it with the placeholder.
                    if (tempMetricNumber < minThreshold) {
                        $('#thresholdWarningNotice').empty().append('<div id="failPlaceholder"> </div>');
                    }
                }
            });
            lightColourArray.pop(lightColourArray.length - 1);
        }
        else {

            // Assign the retrieved value to a global float variable.
            var num = 0;
            num = parseFloat(localStorage.getItem(objectClassName + metricType));

            // Append the new data into the correct page component.
            $('#' + metricType + 'Metric').empty();

            // Add the retrieved metric to the total count for that metric.
            if (metricType === 'KWh') {
                kWhMetricNumber = kWhMetricNumber - num;
                // Bugfix: kWh should never be below 0. If it does fall below 0, set it to 0.
                if (kWhMetricNumber < 0) {
                    kWhMetricNumber = 0;
                }
                $('#KWhMetric').append("Power consumption: <span class=\"metricData\">" + kWhMetricNumber + "kWh</span>");
            }
            if (metricType === 'Co2') {
                co2MetricNumber = co2MetricNumber - num;
                // Bugfix: co2 should never be below 0. If it does fall below 0, set it to 0.
                if (co2MetricNumber < 0) {
                    co2MetricNumber = 0;
                }
                $('#Co2Metric').append("Carbon dioxide output: <span class=\"metricData\">" + co2MetricNumber + " Tonnes per year</span>");
            }
            if (metricType === 'Temp') {
                tempMetricNumber = tempMetricNumber - num;
                $('#TempMetric').append("Current data centre temperature: <span class=\"metricData\">" + tempMetricNumber + "&deg;C</span>");

                // If the temp has fallen belowe the threshold, remove the 
                // warning message and replace it with the placeholder.
                if (tempMetricNumber < minThreshold) {
                    $('#thresholdWarningNotice').empty().append('<div id="failPlaceholder"> </div>');
                }
            }
        }
    };

    /**
     * Resets all totals to zero when undo is called, and there are no more 
     * steps to undo.
     * 
     * @returns {undefined}
     */
    ObjectMetricController.prototype.resetAllToZero = function () {

        // Empty tempThresholdArray by reinitialising it.
        tempThresholdArray = new Array();
        co2MetricNumber = 0;
        kWhMetricNumber = 0;
        tempMetricNumber = 0;
        $('#KWhMetric').empty().append('Power consumption: <span class="metricData">' + kWhMetricNumber + 'kWh</span>');
        $('#Co2Metric').empty().append('Carbon dixoide output: <span class="metricData">' + co2MetricNumber + ' Tonnes per year</span>');
        $('#TempMetric').empty().append('Current data centre temperature: <span class="metricData">' + tempMetricNumber + '&deg;C</span>');
        $('#tempThreshold').empty().append('Data centre maximum temperature: <span class="metricData">N/A</span>');
    };

    /**
     * Removes the last added lowest temperature threshold value from the array 
     * of those values.
     * 
     * @param {type} count The count of objects currently in play.
     * @returns {undefined}
     */
    ObjectMetricController.prototype.popFromThresholdArray = function (count) {
        if (count > 1) {
            tempThresholdArray.pop();
            $('#tempThreshold').empty();
            $('#tempThreshold')
                    .append('Data centre maximum temperature: <span class="metricData">'
                            + tempThresholdArray[tempThresholdArray.length - 1].valueOf()
                            + '&deg;C</span>');
        }
        else {
            clearThresholdArray();
        }
    };

    /**
     * Remove all threshold values from tempThresholdArray.
     * 
     * @returns {undefined}
     */
    ObjectMetricController.prototype.clearThresholdArray = function () {
        for (var i = tempThresholdArray.length - 1; i >= 0; i--) {
            tempThresholdArray.pop();
        }
    };

    // Getters.

    ObjectMetricController.prototype.getKWhMetricNumber = function () {
        return kWhMetricNumber;
    };

    ObjectMetricController.prototype.getCo2MetricNumber = function () {
        return co2MetricNumber;
    };
    
    ObjectMetricController.prototype.getTempMetricNumber = function () {
        return tempMetricNumber;
    };

    ObjectMetricController.prototype.getTempThreshold = function () {
        return tempThresholdArray[tempThresholdArray.length - 1];
    };

};