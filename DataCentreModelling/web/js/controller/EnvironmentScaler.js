/**
 * Resets the number of floors that Environment will add to the scene.
 * 
 * @returns {undefined}
 */
function resetLevelsNum() {
    var levelsNum = $('#setLevelsNum').val();
    localStorage.setItem('levelsNum', levelsNum);
}

/**
 * Called when the user presses the "Reset Levels" button on 
 * EnvironmentResizer.html. Calls resetLevelsNum before alerting the user to 
 * the change that has been made to the number of floors.
 * 
 * @returns {undefined}
 */
function resetLevels() {
    resetLevelsNum();
    alert('Number of levels reset. Now click "Back to the app", or make more changes.');
}

/**
 * Resets the size of the floors added to the scene by Environment.
 * 
 * @returns {undefined}
 */
function resetFloorSpace() {
    var floorSpace = $('#setFloorSpace').val();
    localStorage.setItem('floorSpace', floorSpace);
}

/**
 * Called when the user presses the "Reset Levels" button on 
 * EnvironmentResizer.html. Calls resetFloorSpace before alerting the user to 
 * the change that has been made to the size of the floors.
 * 
 * @returns {undefined}
 */
function resetSpace() {
    resetFloorSpace();
    alert('Floor space reset. Now click "Back to the app", or make more changes.');
}

/**
 * Resets the height between floors, and adds that height above the top floor 
 * to the walls-box added to the scene by Environment.
 * 
 * @returns {undefined}
 */
function resetCeilingHeight() {
    var ceilingHeight = $('#setCeilingHeight').val();
    localStorage.setItem('ceilingHeight', ceilingHeight);
}

/**
 * Called when the user presses "Reset Height". Calls resetCeilingHeight before 
 * alerting the user to the change that has been made to the ceiling height.
 * 
 * @returns {undefined}
 */
function resetHeight() {
    resetCeilingHeight();
    alert('Ceiling height reset. Now click "Back to the app", or make more changes.');
}

/**
 * Called when the user preses "Reset All". Calls resetLevelsNum, 
 * resetFloorSpace and resetCeilingHeight before redirecting the user to
 * index.html.
 * 
 * @returns {undefined}
 */
function resetAll() {
    resetLevelsNum();
    resetFloorSpace();
    resetCeilingHeight();
    window.open('index.html');
}