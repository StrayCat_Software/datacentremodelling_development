/**
 * This is an unimplemented stub for a future AJAX data pre-loader class which
 * would retrieve the required HTML elements from VCSynapse.jsp, and write them
 * to LocalStorage.
 * 
 * @author Daniel Pawsey
 * 
 * @returns {undefined}
 */
var DataReciever = function () {
    
    var page = 'VCSynapse.jsp';

    this.preLoader = function(){
        
    };
    
    /**
     * Retrieve itemName HTML data from the page and add it to LocalStorage.
     * 
     * @param {type} itemName
     * @returns {undefined}
     */
    this.getItem = function (itemName) {
        $.get(page, function (data) {
            var text = $(data).find('#' + itemName).text();
            localStorage.setItem(itemName, text);
        });
    };
    
    /**
     * Retrieve buttons from LocalStorage.
     * @returns {undefined}
     */
    this.getFromStorage = function () {
        alert(localStorage.getItem("buttons"));
    };
};

/**
 * Test of DataReciver.
 * 
 * @returns {undefined}
 */
function test() {
    alert('test() called');
    var dr = new DataReciever();
    dr.getButtons();
    dr.getFromStorage();
}