/**
 * A test of the jQuery AJAX .get function.
 * @returns {undefined}
 */
function ajaxGetTest() {
    $.get('VCSynapse.jsp', function(content) {
        var text = $(content).find('#ServerButtons').html();
        alert(text);
    });
}