<%-- 
    Document   : VCSynapse
    Created on : 04-Jun-2015, 20:10:40
    Author     : Daniel Pawsey
--%>

<%@page import="utils.SiteUtils"%>
<%@page import="mvc.controller.ServletProxy"%>
<%
    ServletProxy sp = new ServletProxy();
    String buttons = sp.getObjectButtons();
    String getObjectNameList = sp.getObjectNameList();
    String metricsNameList = sp.generateObjectMetricNameList();
    String allObjectMetrics = sp.getAllObjectMetrics();
    String jsonModels = sp.makeJSONModels();
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>VCSynapse</title>
    </head>
    <body>
        <%=buttons%>
        <%=getObjectNameList%>
        <%=metricsNameList%>
        <%=allObjectMetrics%>
        <%=jsonModels%>
    </body>
</html>
